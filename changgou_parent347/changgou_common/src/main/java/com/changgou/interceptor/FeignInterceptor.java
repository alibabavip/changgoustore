package com.changgou.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/6
 * @description ：自定义拦截器 将所有请求中都加入令牌
 * @version: 1.0
 */
@Component
public class FeignInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {

        //HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

        if (requestAttributes != null) {

            HttpServletRequest request = requestAttributes.getRequest();
            if (request != null) {

                Enumeration<String> headerNames = request.getHeaderNames();
                if (headerNames != null) {

                    while (headerNames.hasMoreElements()) {
                        String headerName = headerNames.nextElement();
                        if ("authorization".equals(headerName)) {
                            // Bearer jwt
                            String headerValue = request.getHeader(headerName);

                            //向下传递令牌
                            requestTemplate.header(headerName, headerValue);
                        }
                    }
                }
            }
        }

    }
}

