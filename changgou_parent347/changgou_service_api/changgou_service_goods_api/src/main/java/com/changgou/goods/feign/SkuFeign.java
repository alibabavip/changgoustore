package com.changgou.goods.feign;

import com.changgou.entity.Result;
import com.changgou.pojo.Sku;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author liJingJie
 */
@FeignClient(name = "goods")
@RequestMapping("/sku")
public interface SkuFeign {
    /**
     * 多条件搜索品牌数据
     *
     * @param searchMap
     * @return
     */
    @GetMapping(value = "/search")
    public Result findList(@RequestParam Map searchMap);

    /**
     * 根据商品id获取对应的库存集合数据,为生成静态化页面提供服务
     *
     * @param spuid
     * @return
     */
    @GetMapping(value = "/spuid/{spuid}")
    public List<Sku> findListBySpuId(@PathVariable("spuid") String spuid);

    /**
     * 根据id查询sku信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Sku> findById(@PathVariable("id") String id);

    /**
     * 根据用户名递减库存
     *
     * @param username
     * @return
     */
    @PostMapping(value = "/decr/count")
    public Result decrCount(@RequestParam("username") String username);

    /**
     * 根据商品Id和数量恢复商品
     *
     * @param skuId
     * @param num
     */
    @RequestMapping("/resumeSkuStockCount")
    public Result resumeSkuStockCount(@RequestParam("skuId") String skuId, @RequestParam("num") Integer num);
}
