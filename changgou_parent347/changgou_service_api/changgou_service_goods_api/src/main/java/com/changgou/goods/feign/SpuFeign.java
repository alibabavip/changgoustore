package com.changgou.goods.feign;

import com.changgou.pojo.Spu;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author liJingJie
 */
@FeignClient(name = "goods")
@RequestMapping("/spu")
public interface SpuFeign {
    /**
     * 根据商品id获取商品对象,为生成静态页feign调用提供服务
     *
     * @param id
     * @return
     */
    @GetMapping("/spuid/{id}")
    public Spu findByspuId(@PathVariable("id") String id);
}
