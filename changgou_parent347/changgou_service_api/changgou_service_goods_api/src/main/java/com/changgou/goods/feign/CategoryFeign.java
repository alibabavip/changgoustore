package com.changgou.goods.feign;

import com.changgou.pojo.Category;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author liJingJie
 */
@FeignClient(name ="goods" )
@RequestMapping("/category")
public interface CategoryFeign {
    /**
     * 根据分类ID获取分类对象,为生成静态页feign调用提供服务
     *
     * @param id
     * @return
     */
    @GetMapping("/categoryid/{id}")
    public Category findByCategoryId(@PathVariable("id") Integer id);
}
