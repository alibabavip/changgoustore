package com.changgou.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/25
 * @description ：自定义商品实体类 包括商品对象和对应的库存集合对象
 * @version: 1.0
 */
public class Goods implements Serializable {
    //商品对象
    private Spu spu;
    //库存集合对象
    private List<Sku> skuList;

    public Spu getSpu() {
        return spu;
    }

    public void setSpu(Spu spu) {
        this.spu = spu;
    }

    public List<Sku> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<Sku> skuList) {
        this.skuList = skuList;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "spu=" + spu +
                ", skuList=" + skuList +
                '}';
    }
}
