package com.changgou.pojo;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/25
 * @description ：品牌和分类关联表 这张表的主键叫做联合主键
 * @version: 1.0
 */
@Table(name = "tb_category_brand")
public class CategoryAndBrand implements Serializable {
    @Id
    private Integer categoryId;
    @Id
    private Integer brandId;

    @Override
    public String toString() {
        return "CategoryAndBrand{" +
                "categoryId=" + categoryId +
                ", brandId=" + brandId +
                '}';
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }
}
