package com.changgou.seckill.feign;

import com.changgou.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/15
 * @version: 1.0
 */
@FeignClient(name = "seckill")
public interface SecKillOrderFeign {
    /**
     * 秒杀下单
     *
     * @param time
     * @param id
     * @return
     */
    @RequestMapping("/seckillorder/add")
    public Result add(@RequestParam("time") String time, @RequestParam("id") Long id);
}
