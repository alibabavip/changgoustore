package com.changgou.user.pojo;

import javax.persistence.Table;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/8
 * @description ：积分日志
 * @version: 1.0
 */
@Table(name="tb_point_log")
public class PointLog {
    private String orderId;
    private String userId;
    private Integer point;

    @Override
    public String toString() {
        return "PointLog{" +
                "orderId='" + orderId + '\'' +
                ", userId='" + userId + '\'' +
                ", point=" + point +
                '}';
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }
}
