package com.changgou.user.feign;

import com.changgou.entity.Result;
import com.changgou.user.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author liJingJie
 */
@FeignClient(name = "user")
public interface UserFeign {
    /**
     * 获取用户信息
     *
     * @param username 用户名
     * @return
     */
    @GetMapping("/user/load/{username}")
    public User findUserInfo(@PathVariable("username") String username);

    /**
     * 添加积分
     *
     * @param points
     * @return
     */
    @GetMapping(value = "user/points/add")
    public Result addPoints(@RequestParam(value = "points") Integer points);
}
