package com.changgou.user.feign;

import com.changgou.entity.Result;
import com.changgou.user.pojo.Address;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author liJingJie
 */
@FeignClient(name = "user")
public interface AddressFeign {
    /**
     * 获取用户地址集合
     *
     * @return
     */
    @GetMapping("/address/list")
    public Result<List<Address>> list();
}
