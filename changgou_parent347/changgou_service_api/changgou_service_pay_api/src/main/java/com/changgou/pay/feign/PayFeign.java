package com.changgou.pay.feign;

import com.changgou.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author LJJ
 */
@FeignClient(name = "pay")
public interface PayFeign {
    /**
     * 调用支付功能 生成微信支付二维码
     *
     * @param orderId
     * @param payMoney
     * @return
     */
    @GetMapping("/wxpay/nativePay")
    public Result nativePay(@RequestParam("orderId") String orderId, @RequestParam("money") Integer payMoney);

    /**
     * 关闭微信订单
     *
     * @param orderId
     * @return
     */
    @PutMapping("/wxpay/close/{orderId}")
    public Result<Map> closeOrder(@PathVariable("orderId") String orderId);

    /**
     * 查询微信订单
     *
     * @param orderId
     * @return
     */
    @GetMapping("/wxpay/query/{orderId}")
    public Result<Map> queryOrder(@PathVariable("orderId") String orderId);
}
