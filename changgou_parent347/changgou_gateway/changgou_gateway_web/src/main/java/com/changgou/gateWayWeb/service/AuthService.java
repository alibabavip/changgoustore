package com.changgou.gateWayWeb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/5
 * @description ：获取jti和jwt
 * @version: 1.0
 */
@Service
public class AuthService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 从cookie中获取jti
     *
     * @param request
     * @return
     */
    public String getJtiFromCookie(ServerHttpRequest request) {
        HttpCookie cookie = request.getCookies().getFirst("uid");
        if (cookie != null) {
            String jti = cookie.getValue();
            return jti;
        }
        return null;
    }

    /**
     * 从redis中获取jwt
     *
     * @param jti
     * @return
     */
    public String getJwtFromRedis(String jti) {
        String jwt = stringRedisTemplate.boundValueOps(jti).get();
        return jwt;
    }
}
