package com.changgou.gateWayWeb.filter;

import com.changgou.gateWayWeb.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/5
 * @description ：全局网关过滤器对头信息进行增强
 * @version: 1.0
 */
@Component
public class AuthFilter implements GlobalFilter, Ordered {
    public static final String Authorization = "Authorization";

    @Autowired
    private AuthService authService;

    private static final String LOGINURL="http://localhost:9200/oauth/toLogin";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        /**
         * 1)  登录请求放行
         * 2） 从cookie中获取jti，如果没有，拒绝访问
         * 3） 根据jti从redis中获取jwt，如果没有，拒绝访问
         * 4） 对当前请求的头信息进行增强，（Authorization   Bearer jwt）
         */
        ServerHttpRequest request = exchange.getRequest();

        //1)  登录请求放行
        // /api/user/list
        String path = request.getURI().getPath();
        if ("/api/oauth/login".equals(path) || !UrlFilter.hasAuthorize(path)) {
            /**
             * 放行
             */
            return chain.filter(exchange);
        }

        //2） 从cookie中获取jti，如果没有，拒绝访问
        String jti = authService.getJtiFromCookie(request);
        if (StringUtils.isEmpty(jti)) {
            //拒绝访问
            /*response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();*/
            return this.toLoginPage(LOGINURL + "?FROM=" + request.getURI(), exchange);
        }

        //3) 根据jti从redis中获取jwt，如果没有，拒绝访问
        String jwt = authService.getJwtFromRedis(jti);   // 令牌码
        if (StringUtils.isEmpty(jwt)) {
            //拒绝访问
            /*response.setStatusCode(HttpStatus.UNAUTHORIZED);
             return response.setComplete();*/
            return this.toLoginPage(LOGINURL, exchange);
        }

        // 对当前请求的头信息进行增强
        request.mutate().header(Authorization, "Bearer " + jwt);

        return chain.filter(exchange);
    }

    /**
     * 在没有令牌的情况下，跳转登录页面
     *
     * @param loginurl 登录地址
     * @param exchange
     * @return
     */
    private Mono<Void> toLoginPage(String loginurl, ServerWebExchange exchange) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.SEE_OTHER);
        response.getHeaders().set("Location", loginurl);
        return response.setComplete();
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
