package com.changgou.filter;

import com.changgou.util.JWTUtil;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/26
 * @description ：用户鉴权过滤器, 拦截所有请求, 判断用户是否有权限访问我们的各种微服务
 * @version: 1.0
 */
@Component
public class AuthFileter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1.获取请求
        ServerHttpRequest request = exchange.getRequest();
        //2. 获取响应
        ServerHttpResponse response = exchange.getResponse();
        //3.获取访问的uri路径,如果是要去登录的路径就放行
        if (request.getURI().getPath().contains("/admin/login")) {
            return chain.filter(exchange);
        }
        //4.获取请求头信息,从请求头中获取token令牌
        String token = request.getHeaders().getFirst("token");
        //5.如果请求头中没有令牌则返回失败信息
        if (StringUtils.isEmpty(token)) {
            //设置状态码401
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            //跳过当前的响应
            return response.setComplete();
        }
        try {
            //6. 如果请求头中有令牌, 则使用jwt我们定义的秘钥解析令牌
            JWTUtil.parseJWT(token);
        } catch (Exception e) {
            e.printStackTrace();
            //7. 如果解析失败, 则证明令牌是伪造的, 或者超时, 后者被篡改了, 返回失败信息
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //8. 继续执行, 让请求放行
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 3;
    }
}
