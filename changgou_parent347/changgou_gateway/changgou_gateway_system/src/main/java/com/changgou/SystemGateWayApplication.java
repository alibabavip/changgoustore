package com.changgou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author lJ
 */
@SpringBootApplication
@EnableEurekaClient
public class SystemGateWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SystemGateWayApplication.class, args);
    }

    /**
     * 获取访问用户的IP地址, 做限流使用,令牌桶
     * @return
     */
    @Bean
    public KeyResolver ipKeyResolver(){
        //lambda简化写法
        return exchange -> {
            //返回从请求中获取到的访问者的Ip地址
            return Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
        };
        /*
        return new KeyResolver() {
            @Override
            public Mono<String> resolve(ServerWebExchange exchange) {
                //返回从请求中获取到的访问者的Ip地址
                return Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
            }
        };
        */
    }
}
