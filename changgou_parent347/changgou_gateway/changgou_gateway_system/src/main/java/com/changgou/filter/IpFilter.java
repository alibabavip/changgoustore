package com.changgou.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/26
 * @description ：
 * @version: 1.0
 */
public class IpFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //获取请求
        ServerHttpRequest request = exchange.getRequest();
        System.out.println("=====getHostName=====" + request.getRemoteAddress().getHostName());
        System.out.println("=====getAddress=====" + request.getRemoteAddress().getAddress());
        return chain.filter(exchange);
    }

    /**
     * 如果网关中有多个过滤器,数字越小越早执行
     *
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
