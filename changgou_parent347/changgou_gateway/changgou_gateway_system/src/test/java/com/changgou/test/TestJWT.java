package com.changgou.test;

import com.changgou.SystemGateWayApplication;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.UUID;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/26
 * @description ：
 * @version: 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SystemGateWayApplication.class)
public class TestJWT {
    @Test
    public void testCreateJWT() {
        long currentTimeMillis = System.currentTimeMillis();
        JwtBuilder jwtBuilder = Jwts.builder()
                //jwt的唯一标识id
                .setId(UUID.randomUUID().toString())
                //主题(用户名),也可以是json
                .setSubject("我是管理员")
                //jwt这个字符串的创建时间
                .setIssuedAt(new Date())
                //设置令牌的超时时间
                .setExpiration(new Date(currentTimeMillis + 10000000))
                //设置签名的加密算法和秘钥
                .signWith(SignatureAlgorithm.HS256, "itcast");
        //打印生成的jwt令牌字符串
        System.out.println("-------------"+ jwtBuilder.compact());
    }
    @Test
    public void testParseJWT(){
        String jwtStr = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwNzY1MjQ2MC04MjdkLTRiODAtYjlmNC1jYzcyOGI0ZDI4ODQiLCJzdWIiOiLmiJHmmK_nrqHnkIblkZgiLCJpYXQiOjE1NjQxNDQ1NTUsImV4cCI6MTU2NDE1NDU1NX0.QH1NHiddQP82JDKvzxWZa1rRHmdvHdu-AQYMLYx0ULs";
        //解析字符串
        Claims claims = Jwts.parser().setSigningKey("itcast").parseClaimsJws(jwtStr).getBody();
        System.out.println("====" + claims);
    }
}
