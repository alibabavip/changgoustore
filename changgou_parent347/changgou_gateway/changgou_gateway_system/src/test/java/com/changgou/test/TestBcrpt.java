package com.changgou.test;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/26
 * @description ：
 * @version: 1.0
 */
public class TestBcrpt {
    public static void main(String[] args) {
        //这个是盐 29个字符，随机生成
        String gensalt = BCrypt.gensalt();
        String hashpw = BCrypt.hashpw("123456", gensalt);
        System.out.println("=====" + hashpw);
        //校验密码是否正确
        boolean checkpw = BCrypt.checkpw("123456", "$2a$10$jg1qTEFzCuQvn8qHo5GcI.mwtB8mR0GGKJuoa.eLjGM.V/wYUmOfy");
        System.out.println("=====" + checkpw);

    }


}
