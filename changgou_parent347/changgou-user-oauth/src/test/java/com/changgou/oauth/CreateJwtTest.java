package com.changgou.oauth;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;

import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CreateJwtTest {
    /**
     * 基于私钥生成jwt
     */
    @Test
    public void creatJwt() {
        //访问证书文件路径
        ClassPathResource resource = new ClassPathResource("changgou.jks");
        String password = "changgou";
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource, password.toCharArray());
        //获取私钥
        String alias = "changgou";
        String key_password = "changgou";
        //读取公钥和私钥
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair(alias, key_password.toCharArray());
        //获取私钥
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyPair.getPrivate();
        Map info = new HashMap();
        info.put("company", "alibaba");
        info.put("name", "lijingjie");
        //生成jwt
        Jwt jwt = JwtHelper.encode(JSON.toJSONString(info), new RsaSigner(rsaPrivateKey));
        String encoded = jwt.getEncoded();
        //eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoibGlqaW5namllIiwiY29tcGFueSI6ImFsaWJhYmEifQ.a7qSR_LVHGHy6lAkTlZK96NkEYagwwfDTxmHHAMXzRZtLfVt6-jUzD4OhnkG1svSC0_WAXej-v8YYjt-RnKLmUMOMc7WfEGubI9n-drwOPHT8YR6TWNN1q4MRvGbCDjcGK6Sqj6WCxLJn3WfBqhXW3WQ1qV8b-GziiZMG855QLfQovUyHFDa2LNne2XRr-FW2ymJjAOQPT_2clyQoe5BI6kHBuBy1fQXvTq84rqwqnvtyLAk6TJLnSJ84_vHwkKfRSOn-jw8myDsvxl_smmAM-onnRcDh1dKbNx0vvvsWElw0MuWRhBMP7p4ThOGb4oh_qrrPybIoynYLTnSCkQABg
        System.out.println(encoded);
    }

}
