package com.changgou.oauth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ParseJwtTest {
    @Test
    public void parsrJwt(){
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoid2Vpemhhb2h1aSIsImNvbXBhbnkiOiJpdGhlaW1hIn0.exGhWm8wwRPibA_NgVTM4xki6JEwJvTL2hcUSX-esanaoPPy6_EoPpAtq9BjakhGH3Obw3B3phkswwqMQxX3jQpZEn7XoeAoKUlf51F3rxTTmAku_0iDmDKdQ3zjPS6vpPH_1bEHzKx5DbOPQsmIYGvvjRKRp7w8ILMtpF3qwYJnJZBUmUVpEvUiVIVIpt-ZqG9CCNLxGWExcB8_bkQPvwY4kjUoGM0USWaaES9FGY7oYWPJ9NenA6CDH23cKe7G0mDJgJ1IwTz4X3s8A7gbdugchR2rZe9a1RG8D7_WzrV3j9Zm6QO6Jipn2kI4OQscFNiyNiwX4JcrYhfsrbXkSw";
        String publickey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvFsEiaLvij9C1Mz+oyAmt47whAaRkRu/8kePM+X8760UGU0RMwGti6Z9y3LQ0RvK6I0brXmbGB/RsN38PVnhcP8ZfxGUH26kX0RK+tlrxcrG+HkPYOH4XPAL8Q1lu1n9x3tLcIPxq8ZZtuIyKYEmoLKyMsvTviG5flTpDprT25unWgE4md1kthRWXOnfWHATVY7Y/r4obiOL1mS5bEa/iNKotQNnvIAKtjBM4RlIDWMa6dmz+lHtLtqDD2LF1qwoiSIHI75LQZ/CNYaHCfZSxtOydpNKq8eb1/PGiLNolD4La2zf0/1dlcr5mkesV570NxRmU1tFm8Zd3MZlZmyv9QIDAQAB-----END PUBLIC KEY-----";
        Jwt jwt = JwtHelper.decodeAndVerify(token,new RsaVerifier(publickey));
        System.out.println(jwt);
        String claims = jwt.getClaims();
        System.out.println(claims);
    }
}
