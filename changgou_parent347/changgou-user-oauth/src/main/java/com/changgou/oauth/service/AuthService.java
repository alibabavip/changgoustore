package com.changgou.oauth.service;

import com.changgou.oauth.util.AuthToken;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/3
 * @description ：申请令牌
 * @version: 1.0
 */
public interface AuthService {
    /**
     * @param username     用户名
     * @param password     用户密码
     * @param clientId     客户端id
     * @param clientSecret 客户端秘钥
     * @return
     */
    AuthToken applyToken(String username, String password, String clientId, String clientSecret);
}
