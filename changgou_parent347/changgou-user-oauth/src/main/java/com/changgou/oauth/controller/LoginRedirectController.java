package com.changgou.oauth.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lijingjie
 */
@Controller
@RequestMapping("/oauth")
public class LoginRedirectController {

    @RequestMapping("/toLogin")
    public String toLogin(@RequestParam(value = "FROM" ,required = false,defaultValue = "") String from, Model model){
        model.addAttribute("from",from);
        return "login";
    }
}
