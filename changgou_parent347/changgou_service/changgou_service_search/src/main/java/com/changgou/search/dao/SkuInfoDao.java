package com.changgou.search.dao;

import com.changgou.search.pojo.SkuInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

/**
 * @author liJingJie
 */
public interface SkuInfoDao extends ElasticsearchCrudRepository<SkuInfo, Long> {
}
