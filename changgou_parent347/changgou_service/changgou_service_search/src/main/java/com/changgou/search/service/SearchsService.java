package com.changgou.search.service;

import java.util.Map;

/**
 * @author liJingJie
 */
public interface SearchsService {
    /**
     * 搜索
     *
     * @param paramMap
     * @return
     */
    public Map search(Map<String, String> paramMap);
}
