package com.changgou.search.service;

import com.alibaba.fastjson.JSON;
import com.changgou.entity.Result;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.pojo.Sku;
import com.changgou.search.dao.SkuInfoDao;
import com.changgou.search.pojo.SkuInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/29
 * @description ：上架
 * @version: 1.0
 */
@Service
public class EsManagerServiceImpl implements EsManagerService {
    @Autowired
    private ElasticsearchTemplate esTemplate;
    @Autowired
    private SkuInfoDao skuInfoDao;


    @Autowired
    private SkuFeign skuFeign;

    @Override
    public void createMapping() {
        //创建索引库
        esTemplate.createIndex(SkuInfo.class);
        //定义mapping索引库结构
        esTemplate.putMapping(SkuInfo.class);
    }

    @Override
    public void importDataToESBySPUId(String spuId) {
        /**
         * 1.根据spuid查询对应的库存
         */
        Map paramMap = new HashMap<>();
        //设置查询条件,根据商品id查询
        paramMap.put("spuId", spuId);
        //设置审核状态为1, 审核已通过的数据
        paramMap.put("status", 1);
        Result result = skuFeign.findList(paramMap);
        /**
         * 2.将数据导入到es索引库中
         */
        List<Sku> skuList = (List<Sku>) result.getData();
        //将sku对象转成json格式字符串
        String skuJsonStr = JSON.toJSONString(skuList);
        //将json格式字符串转成SkuInfo对象集合
        List<SkuInfo> skuInfos = JSON.parseArray(skuJsonStr, SkuInfo.class);
        if (skuInfos != null) {
            for (SkuInfo skuInfo : skuInfos) {
                //获取规格json字符串,例如:{'颜色': '灰色', '尺码': '200度'}
                String specJsonStr = skuInfo.getSpec();
                //将json格式字符串转成Map
                Map specMap = JSON.parseObject(specJsonStr, Map.class);
                skuInfo.setSpecMap(specMap);
            }
        }
        //将数据放入es索引库
        skuInfoDao.saveAll(skuInfos);
    }

    @Override
    public void importAllDataToES() {
        /**
         * 1.查询索引审核通过的库存数据
         */
        Map paramMap = new HashMap();
        //设置审核状态为1,审核已通过的数据
        paramMap.put("status", 1);
        Result result = skuFeign.findList(paramMap);
        /**
         * 2. 根据数据导入到es索引库中
         */
        List<Sku> skuList = (List<Sku>) result.getData();
        //将sku对象转成json格式字符串
        String skuJsonStr = JSON.toJSONString(skuList);
        //将json格式字符串转成SkuInfo对象集合
        List<SkuInfo> skuInfos = JSON.parseArray(skuJsonStr, SkuInfo.class);
        if (skuInfos != null) {
            for (SkuInfo skuInfo : skuInfos) {
                //获取规格json字符串, 例如: {'颜色': '灰色', '尺码': '200度'}
                String specJsonStr = skuInfo.getSpec();
                //将json格式字符串转成Map
                Map specMap = JSON.parseObject(specJsonStr, Map.class);
                skuInfo.setSpecMap(specMap);
            }
        }
        //将数据放入es索引库
        skuInfoDao.saveAll(skuInfos);
    }
}
