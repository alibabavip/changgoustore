package com.changgou.search.service;

/**
 * @author liJingJie
 */
public interface EsManagerService {
    /**
     * 创建es索引库结构
     */
    void createMapping();

    /**
     * 根据Id把商品导入到Es索引库
     * @param spuId
     */
    void importDataToESBySPUId(String spuId);

    /**
     * 全量导入
     * 导入所有库存表数据到ES索引库
     */
    public void importAllDataToES();
}
