package com.changgou.search.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.search.service.EsManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/29
 * @description ：ES索引库管理
 * @version: 1.0
 */
@RestController
@RequestMapping("/manager")
public class EsManagerController {
    @Autowired
    private EsManagerService esManagerService;

    /**
     * 创建es索引库结构
     *
     * @return
     */
    @GetMapping("/createMapping")
    public Result createMapping() {
        esManagerService.createMapping();
        return new Result(true, StatusCode.OK, "创建索引库结构成功");
    }

    /**
     * 测试导入索引SKU表数据到ES索引库
     *
     * @return
     */
    @GetMapping("/imporAll")
    public Result testImportAllDataToES() {
        esManagerService.importAllDataToES();
        return new Result(true, StatusCode.OK, "导入所有数据到ES成功!");
    }
}
