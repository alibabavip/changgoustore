package com.changgou.search.service;

import com.alibaba.fastjson.JSON;
import com.changgou.search.pojo.SkuInfo;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/31
 * @description ：
 * @version: 1.0
 */
@Service
public class SearchsServiceImpl implements SearchsService {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    /**
     * 设置常量,每页显示20条数据
     */
    public final static Integer PAGE_SIZE = 20;

    @Override
    public Map search(Map<String, String> paramMap) {
        /**
         * 创建或初始化对象
         */
        Map<String, Object> resultMap = new HashMap<>();
        if (resultMap == null) {
            return resultMap;
        }
        //创建查询对象
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        //创建布尔查询对象,(组合条件查询对象,可以将很多查询条件都放进去)
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        /**
         * 根据关键字查询
         */
        if (!StringUtils.isEmpty(paramMap.get("keywords"))) {
            /**
             * mustNot 非 相当于not
             * must    并且 相当于and
             * should  或者 相当于or
             */
            //.operator(Operator.AND)作用是,根据关键字分词后,对切分后的词分别到索引库查询, 查询结果设置成and的关系
            //也就是所有关键字必须匹配才返回结果,模式不设置OR,查询关键字越长,查询结果越多,不够精确
            boolQueryBuilder.must(QueryBuilders.matchQuery("name", paramMap.get("keywords")).operator(Operator.AND));
            //将组合查询对象,放入具体的查询对象实现类中
            nativeSearchQueryBuilder.withQuery(boolQueryBuilder);
        }

        /**
         * 高亮查询
         * 在name域中进行高亮查询,字体设置为红色
         */
        HighlightBuilder.Field hiField = new HighlightBuilder.Field("name").preTags("<em style='color:red'>").postTags("</em>");
        nativeSearchQueryBuilder.withHighlightFields(hiField);

        /**
         * 分页查询
         */
        String pageNum = paramMap.get("pageNum");
        if (StringUtils.isEmpty(pageNum)) {
            pageNum = "1";
            paramMap.put("pageNum", "1");
        }
        resultMap.put("pageNum", pageNum);
        //springDataES中, 第一页是从0开始计算的
        nativeSearchQueryBuilder.withPageable(PageRequest.of(Integer.parseInt(pageNum) - 1, PAGE_SIZE));

        /**
         * 排序
         */
        if (!StringUtils.isEmpty(paramMap.get("sortField"))) {
            //升序
            if ("ASC".equals(paramMap.get("sortRule"))) {
                //创建排序对象
                FieldSortBuilder sortField = SortBuilders.fieldSort(paramMap.get("sortField")).order(SortOrder.ASC);
                //排序对象放入查询对象中
                nativeSearchQueryBuilder.withSort(sortField);
            }
            //降序
            if ("DESC".equals(paramMap.get("sortRule"))) {
                //创建排序对象
                FieldSortBuilder sortField = SortBuilders.fieldSort(paramMap.get("sortField")).order(SortOrder.DESC);
                //排序对象放入查询对象中
                nativeSearchQueryBuilder.withSort(sortField);
            }
        }
        /**
         * 根据品牌过滤查询
         */
        if (!StringUtils.isEmpty(paramMap.get("brand"))) {
            //创建精确查询查询对象,作为过滤条件
            TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("brandName", paramMap.get("brand"));
            //将过滤条件加入到查询对象中
            boolQueryBuilder.filter(termQueryBuilder);
        }

        /**
         * 根据规格过滤查询
         */
        //遍历所有查询参数
        for (String paramKey : paramMap.keySet()) {
            //判断如果参数查询的key,是以spec_开头的都认为是规格过滤条件
            //例如: spec_颜色: 蓝色   spec_版本:windows版  spec_屏幕尺寸:5.5寸
            if (paramKey.startsWith("spec_")) {
                //对规格中的value值中特殊符号进行转换
                String value = paramMap.get(paramKey).replace("%2B", "+");
                //创建精确查询对象,作为过滤条件 .keyword是将规格转换为keyword类型这样才可以聚合, 才可以过滤, 才可以精确查询
                //规格原来默认类型是text类型,只允许切分词,模糊查询,不允许过滤,精确,聚合查询
                TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("specMap." + paramKey.substring(5) + ".keyword", value);
                System.out.println("specMap." + paramKey.substring(5) + ".keyword" + "======" + value);
                //将过滤条件加入到查询对象中
                boolQueryBuilder.filter(termQueryBuilder);
            }
        }
        /**
         * 根据价格过滤查询
         */
        if (!StringUtils.isEmpty(paramMap.get("price"))) {
            //数据举例: 1000-10000
            String[] prices = paramMap.get("price").split("-");
            if (prices != null && prices.length > 0) {
                //大于等于最小值
                boolQueryBuilder.filter(QueryBuilders.rangeQuery("price").gte(prices[0]));
                //小于等于最小值
                boolQueryBuilder.filter(QueryBuilders.rangeQuery("price").lte(prices[1]));
            }
        }
        /**
         * 根据品牌聚合查询(分组)
         * 作用: 根据关键字查询出来的结果中, 找出来对应的所有不重复的品牌数据集合, 展示在页面上作为过滤备选条件使用
         */
        //为品牌聚合起名, 一会下面获取聚合结果的时候通过这个名字取结果
        String brandGroup = "brandGroup";
        TermsAggregationBuilder brandAgg = AggregationBuilders.terms(brandGroup).field("brandName");
        nativeSearchQueryBuilder.addAggregation(brandAgg);
        /**
         * 根据规格聚合查询(分组)
         * 作用: 根据关键字查询出来的结果中, 找出来对应的所有不重复的规格数据集合, 展示在页面上作为过滤备选条件使用
         */
        String specGroup = "specGroup";
        TermsAggregationBuilder specAgg = AggregationBuilders.terms(specGroup).field("spec.keyword");
        nativeSearchQueryBuilder.addAggregation(specAgg);
        /**
         * 查询并获取查询结果
         */
        //在查询条件生成器中生成查询对象所以去build构建
        AggregatedPage<SkuInfo> skuInfos = elasticsearchTemplate.queryForPage(nativeSearchQueryBuilder.build(), SkuInfo.class, new SearchResultMapper() {
            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse searchResponse, Class<T> aClass, Pageable pageable) {
                //从响应中获取查询到的结果集
                SearchHits hits = searchResponse.getHits();
                //创建需要返回的对象
                List<SkuInfo> skuInfoList = new ArrayList<>();
                if (hits != null && hits.getTotalHits() > 0) {
                    for (SearchHit hit : hits) {
                        //获取查询出来的一条json字符串结果
                        String skuInfoJsonStr = hit.getSourceAsString();
                        //将json格式字符串转成对应的java对象
                        SkuInfo skuInfo = JSON.parseObject(skuInfoJsonStr, SkuInfo.class);
                        /**
                         * 获取高亮名称,然后将高亮名称放入skuInfo对象的name属性中, 覆盖以前不带高亮的名称
                         */
                        Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                        if (highlightFields != null && highlightFields.size() > 0) {
                            //获取到高亮名称
                            String highLightName = highlightFields.get("name").fragments()[0].toString();
                            System.out.println(highlightFields.get("name").toString());
                            System.out.println(highlightFields.get("name").fragments()[0].toString());
                            //将高亮的名称,覆盖不带高亮的名称
                            skuInfo.setName(highLightName);
                        }
                        //将对象添加到返回的集合当中
                        skuInfoList.add(skuInfo);
                    }
                }
                /**
                 * 第一个参数:查询到的结果集合,第二个参数.分页对象,第三个参数:查询到的总条数,第四个参数:聚合结果对象
                 */
                return new AggregatedPageImpl(skuInfoList, pageable, hits.getTotalHits(), searchResponse.getAggregations());
            }
        });
        //查询到的总条数
        resultMap.put("total", skuInfos.getTotalElements());
        //查询到的总页数
        resultMap.put("totalPages", skuInfos.getTotalPages());
        //查询出来的结果集
        resultMap.put("rows", skuInfos.getContent());
        /**
         * 获取品牌聚合结果(分组)
         */
        //将查询条件取出,如:华为:华为手机 小米:小米手机
        StringTerms brandTerms = (StringTerms) skuInfos.getAggregation(brandGroup);
        //再将查询到的结果,用键找值取出所有的库存(sku)商品,转换为集合
        List<String> brandList = brandTerms.getBuckets().stream().map(bucket -> bucket.getKeyAsString()).collect(Collectors.toList());
        //一个品牌有多个库存商品,放入品牌结果
        resultMap.put("brandList", brandList);
        /**
         List<String> brandList = new ArrayList<>();
         for (StringTerms.Bucket bucket : brandTerms.getBuckets()) {
         String brandStr = bucket.getKeyAsString();
         brandList.add(brandStr);
         }
         */

        /**
         * 获取规格聚合结果(分组)
         */
        StringTerms specTerms = (StringTerms) skuInfos.getAggregation(specGroup);
        List<String> specList = specTerms.getBuckets().stream().map(bucket -> bucket.getKeyAsString()).collect(Collectors.toList());
        resultMap.put("specList", getSpecSet(specList));
        return resultMap;
    }

    /**
     * 进一步整理规格数据格式,去掉重复的数据,以便在页面遍历使用
     *
     * @param specList 传入聚合查询出来的规格数据
     *                 例如:
     *                 "specList": [
     *                 "{'颜色': '蓝色', '版本': '6GB+128GB'}",
     *                 "{'颜色': '黑色', '版本': '6GB+128GB'}",
     *                 "{'颜色': '黑色', '版本': '4GB+64GB'}",
     *                 "{'颜色': '粉色', '版本': '6GB+128GB'}"
     *                 ]
     * @return
     */
    private Map<String, Set<String>> getSpecSet(List<String> specList) {
        Map<String, Set<String>> resultMap = new HashMap<>();
        if (specList == null) {
            return resultMap;
        }
        //遍历规格数据, 每遍历一次都是{'颜色': '蓝色', '版本': '6GB+128GB'}
        for (String specJsonStr : specList) {
            Map<String, String> specMap = JSON.parseObject(specJsonStr, Map.class);
            //遍历规格Map{'颜色': '蓝色', '版本': '6GB+128GB'}
            for (String specKey : specMap.keySet()) {
                Set<String> resultSet = resultMap.get(specKey);
                if (resultSet == null) {
                    resultSet = new HashSet<>();
                }
                //specMap.get(specKey)是从规格Map中通过key来获取值, 获取到的值为例如: 蓝色, 黑色, 白色.....
                resultSet.add(specMap.get(specKey));
                //将set放入需要返回的Map中, 覆盖里面以前的值
                resultMap.put(specKey, resultSet);
            }
        }
        return resultMap;
    }
}
