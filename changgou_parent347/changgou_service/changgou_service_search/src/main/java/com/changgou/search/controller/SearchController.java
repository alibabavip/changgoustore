package com.changgou.search.controller;

import com.changgou.entity.Page;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;

import com.changgou.search.pojo.SkuInfo;
import com.changgou.search.service.SearchsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.Map;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/31
 * @description ：搜索
 * @version: 1.0
 */
@Controller
@RequestMapping("/search")
public class SearchController {
    @Autowired
    private SearchsService searchsService;

    /**
     * 请求参数处理
     * 因为在+ 在url中是特殊符号,所以需要转义
     *
     * @param paramMap
     */
    public void paramMapHandler(Map<String, String> paramMap) {
        if (paramMap != null) {
            //遍历所有参数的key
            for (String paramKey : paramMap.keySet()) {
                String value = paramMap.get(paramKey).replace(" ", "%2B");
                paramMap.put(paramKey, value);
            }
        }
    }

    /**
     * 搜索
     *
     * @param searchMap
     * @return
     */
    @GetMapping(value = "/list")
    public String search(@RequestParam Map<String, String> searchMap, Model model) {
        //先调用转义方法, 将查询参数中的特殊符号进行处理
        paramMapHandler(searchMap);

        //将查询条件返回给页面进行查询条件回显
        model.addAttribute("searchMap", searchMap);
        //调用service查询数据
        Map resultMap = searchsService.search(searchMap);
        //将结果返回给页面
        model.addAttribute("result", resultMap);

        Page<SkuInfo> page = new Page<>(
                Long.parseLong(String.valueOf(resultMap.get("total"))),
                Integer.parseInt(String.valueOf(resultMap.get("pageNum"))),
                20
        );
        model.addAttribute("page", page);
        /**
         * 拼接url
         */
        StringBuffer url = new StringBuffer("/search/list");
        if (searchMap != null && searchMap.size() > 0) {
            url.append("?");
            //遍历请求参数Map
            for (String searchMapKey : searchMap.keySet()) {
                if (!"pageNum".equals(searchMapKey)
                        && !"sortField".equals(searchMapKey)
                        && !"sortRule".equals(searchMapKey)) {
                    url.append("&").append(searchMapKey).append("=").append(searchMap.get(searchMapKey));
                }
            }
        }
        model.addAttribute("url", url.toString());
        return "search";
    }
}
