package com.changgou.search.listener;

import com.changgou.search.service.EsManagerService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/30
 * @version: 1.0
 * 自定义监听器,监听rabbitMq中的商品上架队列
 * 根据队列中发过来的商品id到goods商品微服务中查询,查询对应的库存集合数据
 * 将库存集合数据导入到es索引库中,供消费者搜索使用
 */
@Component
@RabbitListener(queues = "search_add_queue")
public class SearchListener {
    @Autowired
    private EsManagerService esManagerService;
    @RabbitHandler
    public void getMessage(String spuId){
        esManagerService.importDataToESBySPUId(spuId);
        System.out.println("========导入spuId叫做=====" + spuId +"=====数据成功!");
    }
}
