package com.changgou.user.dao;

import com.changgou.user.pojo.PointLog;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author LJJ
 */
public interface PointLogMapper extends Mapper<PointLog> {
    /**
     * 查询当前订单是否操作过
     *
     * @param orderId
     * @return
     */
    @Select("select * from tb_point_log where order_id=#{orderId}")
    PointLog findPointLogByOrderId(String orderId);
}
