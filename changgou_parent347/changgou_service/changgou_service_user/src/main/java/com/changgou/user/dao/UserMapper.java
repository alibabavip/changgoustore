package com.changgou.user.dao;

import com.changgou.user.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
/*   *//**
     * 增加积分
     *
     * @param username
     * @param point
     * @return
     *//*
    @Update("UPDATE tb_user SET points=points+#{point} WHERE  username=#{username}")
    int addUserPoints(@Param("username") String username, @Param("point") Integer point);*/

    /**
     * 根据用户名修改积分
     *
     * @param userName
     * @param point
     * @return
     */
    @Update("update tb_user set points=points+#{point} where username=#{userName}")
    int updateUserPoint(@Param("userName")String userName,@Param("point") int point);
}
