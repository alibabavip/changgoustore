package com.changgou.user.listener;

import com.alibaba.fastjson.JSON;
import com.changgou.order.pojo.Task;
import com.changgou.user.config.RabbitMQConfig;
import com.changgou.user.service.UserService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/8
 * @description ：监听用户积分变化
 * @version: 1.0
 */
@Component
public class AddPointListener {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = RabbitMQConfig.CG_BUYING_ADDPOINT)
    public void receiveMessage(String message) {
        System.out.println(message);
        System.out.println("用户服务：获取订单任务信息");
        //消息转换
        Task task = JSON.parseObject(message, Task.class);

        //判断redis中是否存在内容
        Object value = redisTemplate.boundValueOps(task.getId()).get();
        if (value != null) {
            return;
        }

        //更新用户积分
        int result = userService.updateUserPoints(task);
        if (result <= 0) {
            return;
        }
        System.out.println("修改用户积分成功");
        //用户更新积分成功，通知订单服务，删除相对应的任务
        rabbitTemplate.convertAndSend(RabbitMQConfig.EX_BUYING_ADDPOINTURSE,RabbitMQConfig.CG_BUYING_FINISHADDPOINT_KEY,JSON.toJSONString(task));
    }
}
