package com.changgou.page.listerner;

import com.changgou.page.service.PageService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/2
 * @description ：生成商品详情页监听器
 * 监听MQ队列中发来的商品id,根据商品id生成商品详情静态化页面
 * @version: 1.0
 */
@Component
@RabbitListener(queues = "page_create_queue")
public class PageListener {
    @Autowired
    private PageService pageService;

    @RabbitHandler
    public void getMessage(String spuId) {
        //调用service根据商品页面生成静态页面
        pageService.createPageHtml(spuId);
    }
}
