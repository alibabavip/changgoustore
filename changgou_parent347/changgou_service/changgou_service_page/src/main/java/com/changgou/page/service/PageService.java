package com.changgou.page.service;

/**
 * @author liJingJie
 */
public interface PageService {
    /**
     * 用模板引擎生成静态化页面
     *
     * @param spuId
     */
    public void createPageHtml(String spuId);
}
