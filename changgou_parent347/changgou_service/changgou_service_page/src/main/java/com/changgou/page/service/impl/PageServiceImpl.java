package com.changgou.page.service.impl;

import com.alibaba.fastjson.JSON;
import com.changgou.goods.feign.CategoryFeign;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.goods.feign.SpuFeign;
import com.changgou.page.service.PageService;
import com.changgou.pojo.Category;
import com.changgou.pojo.Sku;
import com.changgou.pojo.Spu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LJJ
 */
@Service
public class PageServiceImpl implements PageService {

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private SpuFeign spuFeign;

    @Autowired
    private SkuFeign skuFeign;

    @Autowired
    private CategoryFeign categoryFeign;

    //获取配置文件中指定的静态页面生成位置
    @Value("${pagepath}")
    private String pagepath;

    @Override
    public void createPageHtml(String spuId) {
        //1. 创建生成的目录位置以及文件名
        File dir = new File(pagepath);
        //2. 判断生成的目录位置, 是否存在目录, 如果不存在则自动创建目录
        if (!dir.exists()) {
            dir.mkdirs();
        }

        //3. 创建模板引擎需要的数据对象
        Map<String, Object> dataMap = findPageDataBySpuId(spuId);
        Context context = new Context();
        context.setVariables(dataMap);

        Writer writer = null;
        try {
            //4. 创建输出流, 流中指定具体生成的位置和文件名
            writer = new PrintWriter(dir + "/" + spuId +".html");

            //5. 生成
            templateEngine.process("item", context, writer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            //6. 关闭流
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    /**
     * 获取生成静态页面中需要的数据
     * @param spuId 商品id
     * @return
     */
    private Map<String, Object> findPageDataBySpuId(String spuId) {

        Map<String, Object> resultMap = new HashMap<>();

        /**
         * 1. 根据商品id获取商品对象
         */
        Spu spu = spuFeign.findByspuId(spuId);
        resultMap.put("spu", spu);

        //处理商品图片数据
        if (spu != null) {
            String[] imagesList = spu.getImages().split(",");
            resultMap.put("imageList", imagesList);
        }

        /**
         * 2. 根据商品id获取分类对象
         */
        Category category1 = categoryFeign.findByCategoryId(spu.getCategory1Id());
        resultMap.put("category1", category1);
        Category category2 = categoryFeign.findByCategoryId(spu.getCategory2Id());
        resultMap.put("category2", category2);
        Category category3 = categoryFeign.findByCategoryId(spu.getCategory3Id());
        resultMap.put("category3", category3);

        /**
         * 3. 根据商品id获取对应的库存集合对象
         */
        List<Sku> skuList = skuFeign.findListBySpuId(spuId);
        resultMap.put("skuList", skuList);

        /**
         * 4. 根据商品id获对应的规格数据
         */
        Map specMap = JSON.parseObject(spu.getSpecItems(), Map.class);
        resultMap.put("specificationList", specMap);

        return resultMap;
    }



}
