package com.changgou.consume.listener;

import com.alibaba.fastjson.JSON;
import com.changgou.consume.service.SecKillOrderService;
import com.changgou.seckill.pojo.SeckillOrder;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import com.changgou.consume.config.RabbitMQConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/14
 * @description ：秒杀下单监听类
 * @version: 1.0
 */
@Component
public class SecKillOrderListener {

    @Autowired
    private SecKillOrderService secKillOrderService;

    @RabbitListener(queues = RabbitMQConfig.SECKILL_ORDER)
    public void receiveSecKillOrderMessage(Channel channel, Message message) {
        try {
            channel.basicQos(300);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SeckillOrder seckillOrder = JSON.parseObject(message.getBody(), SeckillOrder.class);

        //同步mysql,存储订单信息,扣减秒杀商品库存
        int rows = secKillOrderService.createOrder(seckillOrder);
        if (rows > 0) {
            //更新数据库操作成功
            //消费者服务发起成功通知
            try {
                //DeliveryTag: 消息的唯一标识 channel+消息编号
                //第二个参数：是否开启批量处理 false:不开启批量
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            //更新数据库失败
            //消费者服务发起失败通知
            try {
                //第二个boolean true当前消息会进入到死信队列，false重新回到原有队列中，默认回到头部
                channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
