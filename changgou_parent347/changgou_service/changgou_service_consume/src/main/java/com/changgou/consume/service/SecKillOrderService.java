package com.changgou.consume.service;

import com.changgou.seckill.pojo.SeckillOrder;

/**
 * @author LJJ
 */
public interface SecKillOrderService {
    /**
     * 存储订单信息
     *
     * @param seckillOrder
     * @return
     */
    int createOrder(SeckillOrder seckillOrder);
}
