package com.changgou.consume.service.impl;

import com.changgou.consume.dao.SeckillGoodsMapper;
import com.changgou.consume.dao.SeckillOrderMapper;
import com.changgou.consume.service.SecKillOrderService;
import com.changgou.seckill.pojo.SeckillOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/14
 * @description ：同步数据库
 * @version: 1.0
 */
@Service
public class SecKillOrderServiceImpl implements SecKillOrderService {

    @Autowired
    private SeckillGoodsMapper seckillGoodsMapper;

    @Autowired
    private SeckillOrderMapper seckillOrderMapper;

    @Override
    @Transactional
    public int createOrder(SeckillOrder seckillOrder) {

        //更改库存
        int result = seckillGoodsMapper.updateStockCountById(seckillOrder.getSeckillId());
        if (result <= 0) {
            return result;
        }

        //添加订单
        result = seckillOrderMapper.insertSelective(seckillOrder);
        if (result <= 0) {
            return result;
        }
        return result;
    }
}
