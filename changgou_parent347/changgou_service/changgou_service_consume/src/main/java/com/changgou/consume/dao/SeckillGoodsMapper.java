package com.changgou.consume.dao;

import com.changgou.seckill.pojo.SeckillGoods;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author LJJ
 */
public interface SeckillGoodsMapper extends Mapper<SeckillGoods> {
    /**
     * 秒杀更改库存
     *
     * @param seckillId
     * @return
     */
    @Update("update tb_seckill_goods set stock_count=stock_count-1 where id=#{seckillId}")
    int updateStockCountById(Long seckillId);
}
