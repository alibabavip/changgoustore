package com.changgou.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.changgou.goods.dao.*;
import com.changgou.goods.service.SpuService;
import com.changgou.pojo.*;
import com.changgou.util.IdWorker;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SpuServiceImpl implements SpuService {

    @Autowired
    private SpuMapper spuMapper;
    @Autowired
    private SkuMapper skuMapper;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private BrandMapper brandMapper;
    @Autowired
    private CategoryAndBrandMapper categoryAndBrandMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 查询全部列表
     *
     * @return
     */
    @Override
    public List<Spu> findAll() {
        return spuMapper.selectAll();
    }

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    @Override
    public Spu findById(String id) {
        return spuMapper.selectByPrimaryKey(id);
    }

    @Override
    public Goods findGoodsBySpuId(String id) {
        /**
         * 1.根据商品Id查询商品对象
         */
        Spu spu = spuMapper.selectByPrimaryKey(id);
        /**
         * 2.根据商品Id,查询库存集合对象
         */
        Example example = new Example(Sku.class);
        /**
         * 创建条件查询
         */
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("spuId", id);
        List<Sku> skuList = skuMapper.selectByExample(example);
        /**
         * 3.封装成Goods对象返回
         */
        Goods goods = new Goods();
        goods.setSpu(spu);
        goods.setSkuList(skuList);
        return goods;
    }

    /**
     * 增加
     *
     * @param spu
     */
    @Override
    public void add(Spu spu) {
        spuMapper.insert(spu);
    }

    @Override
    public void addGoods(Goods goods) {
        /**
         * 1.根据商品对象,添加商品
         */
        //生成商品唯一Id
        long spuId = idWorker.nextId();
        goods.getSpu().setId(String.valueOf(spuId));
        spuMapper.insertSelective(goods.getSpu());
        /**
         * 2.关联品牌和分类数据
         * 首先查看这个品牌和分类有没有关联关系,如果没有将这关联关系数据插入到分类和品牌的关联表中
         */
        CategoryAndBrand categoryAndBrand = new CategoryAndBrand();
        categoryAndBrand.setBrandId(goods.getSpu().getBrandId());
        categoryAndBrand.setCategoryId(goods.getSpu().getCategory3Id());
        int count = categoryAndBrandMapper.selectCount(categoryAndBrand);
        if (count == 0) {
            categoryAndBrandMapper.insertSelective(categoryAndBrand);
        }
        /**
         * 3. 添加库存集合
         */
        saveSkuList(goods);
    }

    /**
     * 修改
     *
     * @param spu
     */
    @Override
    public void update(Spu spu) {
        spuMapper.updateByPrimaryKey(spu);
    }

    @Override
    public void updateGoods(Goods goods) {
        /**
         * 1.根据商品Id,修改商品对象
         */
        spuMapper.updateByPrimaryKeySelective(goods.getSpu());
        /**
         * 2.根据是商品Id删除对应的库存集合数据
         */
        Example example = new Example(Sku.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("spuId", goods.getSpu().getId());
        skuMapper.deleteByExample(example);
        /**
         * 3.重新添加库存集合数据到库存表
         */
        saveSkuList(goods);
    }

    /**
     * 添加库存集合
     *
     * @param goods
     */
    private void saveSkuList(Goods goods) {
        List<Sku> skuList = goods.getSkuList();
        if (skuList != null) {
            for (Sku sku : skuList) {
                //生成全局id
                long skuId = idWorker.nextId();
                sku.setId(String.valueOf(skuId));
                //库存名称 起始为商品名称
                String title = goods.getSpu().getName();
                //获取库存规格
                String specJsonStr = sku.getSpec();
                if (specJsonStr != null) {
                    //解析库存规格json字符串
                    Map<String, String> specMap = JSON.parseObject(specJsonStr, Map.class);
                    for (String specValue : specMap.values()) {
                        //将规格的值拼接到库存名称中
                        title += " " + specValue;
                    }
                }
                sku.setName(title);
                //规格
                if (StringUtils.isEmpty(sku.getSpec())) {
                    sku.setSpec("{}");
                }
                //创建时间
                sku.setCreateTime(new Date());
                //更新时间
                sku.setUpdateTime(new Date());
                //分类id
                sku.setCategoryId(goods.getSpu().getCategory3Id());
                //分类名称
                Category category = categoryMapper.selectByPrimaryKey(goods.getSpu().getCategory3Id());
                sku.setCategoryName(category.getName());
                //商品id
                sku.setSpuId(goods.getSpu().getId());
                //品牌名称
                Brand brand = brandMapper.selectByPrimaryKey(goods.getSpu().getBrandId());
                sku.setBrandName(brand.getName());
                skuMapper.insertSelective(sku);
            }
        }
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void realDelete(String id) {
        /**
         * 1. 根据商品id, 删除商品对象
         */
        spuMapper.deleteByPrimaryKey(id);
        /**
         * 2.根据商品Id,删除库存集合对象
         */
        Example example = new Example(Sku.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("spuId", id);
        skuMapper.deleteByExample(example);
    }


    /**
     * 条件查询
     *
     * @param searchMap
     * @return
     */
    @Override
    public List<Spu> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return spuMapper.selectByExample(example);
    }

    /**
     * 分页查询
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public Page<Spu> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        return (Page<Spu>) spuMapper.selectAll();
    }

    /**
     * 条件+分页查询
     *
     * @param searchMap 查询条件
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @Override
    public Page<Spu> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page, size);
        Example example = createExample(searchMap);
        return (Page<Spu>) spuMapper.selectByExample(example);
    }

    /**
     * 构建查询对象
     *
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap) {
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        if (searchMap != null) {
            // 主键
            if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                criteria.andEqualTo("id", searchMap.get("id"));
            }
            // 货号
            if (searchMap.get("sn") != null && !"".equals(searchMap.get("sn"))) {
                criteria.andEqualTo("sn", searchMap.get("sn"));
            }
            // SPU名
            if (searchMap.get("name") != null && !"".equals(searchMap.get("name"))) {
                criteria.andLike("name", "%" + searchMap.get("name") + "%");
            }
            // 副标题
            if (searchMap.get("caption") != null && !"".equals(searchMap.get("caption"))) {
                criteria.andLike("caption", "%" + searchMap.get("caption") + "%");
            }
            // 图片
            if (searchMap.get("image") != null && !"".equals(searchMap.get("image"))) {
                criteria.andLike("image", "%" + searchMap.get("image") + "%");
            }
            // 图片列表
            if (searchMap.get("images") != null && !"".equals(searchMap.get("images"))) {
                criteria.andLike("images", "%" + searchMap.get("images") + "%");
            }
            // 售后服务
            if (searchMap.get("saleService") != null && !"".equals(searchMap.get("saleService"))) {
                criteria.andLike("saleService", "%" + searchMap.get("saleService") + "%");
            }
            // 介绍
            if (searchMap.get("introduction") != null && !"".equals(searchMap.get("introduction"))) {
                criteria.andLike("introduction", "%" + searchMap.get("introduction") + "%");
            }
            // 规格列表
            if (searchMap.get("specItems") != null && !"".equals(searchMap.get("specItems"))) {
                criteria.andLike("specItems", "%" + searchMap.get("specItems") + "%");
            }
            // 参数列表
            if (searchMap.get("paraItems") != null && !"".equals(searchMap.get("paraItems"))) {
                criteria.andLike("paraItems", "%" + searchMap.get("paraItems") + "%");
            }
            // 是否上架
            if (searchMap.get("isMarketable") != null && !"".equals(searchMap.get("isMarketable"))) {
                criteria.andEqualTo("isMarketable", searchMap.get("isMarketable"));
            }
            // 是否启用规格
            if (searchMap.get("isEnableSpec") != null && !"".equals(searchMap.get("isEnableSpec"))) {
                criteria.andEqualTo("isEnableSpec", searchMap.get("isEnableSpec"));
            }
            // 是否删除
            if (searchMap.get("isDelete") != null && !"".equals(searchMap.get("isDelete"))) {
                criteria.andEqualTo("isDelete", searchMap.get("isDelete"));
            }
            // 审核状态
            if (searchMap.get("status") != null && !"".equals(searchMap.get("status"))) {
                criteria.andEqualTo("status", searchMap.get("status"));
            }

            // 品牌ID
            if (searchMap.get("brandId") != null) {
                criteria.andEqualTo("brandId", searchMap.get("brandId"));
            }
            // 一级分类
            if (searchMap.get("category1Id") != null) {
                criteria.andEqualTo("category1Id", searchMap.get("category1Id"));
            }
            // 二级分类
            if (searchMap.get("category2Id") != null) {
                criteria.andEqualTo("category2Id", searchMap.get("category2Id"));
            }
            // 三级分类
            if (searchMap.get("category3Id") != null) {
                criteria.andEqualTo("category3Id", searchMap.get("category3Id"));
            }
            // 模板ID
            if (searchMap.get("templateId") != null) {
                criteria.andEqualTo("templateId", searchMap.get("templateId"));
            }
            // 运费模板id
            if (searchMap.get("freightId") != null) {
                criteria.andEqualTo("freightId", searchMap.get("freightId"));
            }
            // 销量
            if (searchMap.get("saleNum") != null) {
                criteria.andEqualTo("saleNum", searchMap.get("saleNum"));
            }
            // 评论数
            if (searchMap.get("commentNum") != null) {
                criteria.andEqualTo("commentNum", searchMap.get("commentNum"));
            }

        }
        return example;
    }

    @Override
    public void audit(String spuId) {
        Spu spu = new Spu();
        spu.setId(spuId);
        //将商品的审核状态改为1, 已审核
        spu.setStatus("1");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    @Override
    public void pull(String spuId) {
        Spu spu = new Spu();
        //将上架状态改为, 已下架
        spu.setIsMarketable("0");
        spu.setId(spuId);
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    @Override
    public void put(String spuId) {
        /**
         *1.根据商品id,查询商品对象
         */
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        /**
         * 2.判断商品删除状态,必须为未删除
         */
        if (spu == null || "1".equals(spu.getIsDelete())) {
            throw new RuntimeException("此商品已删除, 不可上架!");
        }
        /**
         * 3.判断商品删除状态.必须是审核已通过
         */
        if (spu == null || "0".equals(spu.getStatus())) {
            throw new RuntimeException("此商品审核未通过, 不可上架!");
        }
        /**
         * 4.商品上架
         */
        //将商品上架状态改为已上架
        spu.setIsMarketable("1");
        spuMapper.updateByPrimaryKeySelective(spu);
        /**
         * 5.商品上架,需要将商品id发送到rabbitMq的上架交换器中
         */
        rabbitTemplate.convertAndSend("goods_up_exchange", "", spuId);
    }

    @Override
    public void delete(String spuId) {
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        //1. 删除的商品必须是已经下架的
        if (spu == null || "1".equals(spu.getIsMarketable())) {
            throw new RuntimeException("已上架的商品不能删除!");
        }
        /**
         * 2.将删除的字段改为1,已删除状态
         */
        spu.setIsDelete("1");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    @Override
    public void restore(String spuId) {
        Spu spu = new Spu();
        spu.setId(spuId);
        //将删除状态改为0, 未删除
        spu.setIsDelete("0");
        spuMapper.updateByPrimaryKeySelective(spu);
    }
}
