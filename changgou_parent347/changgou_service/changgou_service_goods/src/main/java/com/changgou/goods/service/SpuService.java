package com.changgou.goods.service;

import com.changgou.pojo.Goods;
import com.changgou.pojo.Spu;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

public interface SpuService {

    /***
     * 查询所有
     * @return
     */
    List<Spu> findAll();

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Spu findById(String id);

    /**
     * 根据SpuId查询
     *
     * @param id
     * @return
     */
    Goods findGoodsBySpuId(String id);

    /***
     * 新增
     * @param spu
     */
    void add(Spu spu);

    /**
     * 新增商品
     *
     * @param goods
     */
    void addGoods(Goods goods);

    /***
     * 修改
     * @param spu
     */
    void update(Spu spu);

    /**
     * 商品对象Id修改
     *
     * @param goods
     */
    void updateGoods(Goods goods);

    /***
     * 删除
     * @param id
     */
    void realDelete(String id);

    /***
     * 多条件搜索
     * @param searchMap
     * @return
     */
    List<Spu> findList(Map<String, Object> searchMap);

    /***
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    Page<Spu> findPage(int page, int size);

    /***
     * 多条件分页查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    Page<Spu> findPage(Map<String, Object> searchMap, int page, int size);

    /**
     * 商品审核
     *
     * @param spuId
     */
    void audit(String spuId);

    /**
     * 商品下架
     *
     * @param spuId
     */
    void pull(String spuId);

    /**
     * 商品上架
     *
     * @param spuId
     */
    void put(String spuId);

    /**
     * 根据商品id进行逻辑删除
     *
     * @param spuId
     */
    void delete(String spuId);

    /**
     * 对逻辑删除的商品进行恢复
     *
     * @param spuId
     */
    void restore(String spuId);
}
