package com.changgou.goods.dao;

import com.changgou.pojo.Spec;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface SpecMapper extends Mapper<Spec> {
    /**
     * 根据商品分类名称查询规格列表
     *
     * @param categoryName
     * @return
     */
    @Select("SELECT s.name,s.options FROM tb_spec s LEFT JOIN tb_category c ON s.template_id = c.template_id WHERE c.name = #{categoryName}")
    public List<Map> findListByCategoryName(@Param("categoryName") String categoryName);
}
