package com.changgou.goods.dao;

import com.changgou.order.pojo.OrderItem;
import com.changgou.pojo.Sku;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

public interface SkuMapper extends Mapper<Sku> {
    /**
     * 减库存
     *
     * @param orderItem
     * @return
     */
    @Update("update tb_sku set num=num-#{num},sale_num=sale_num+#{num} where id=#{skuId} and num>=#{num}")
    int decrCount(OrderItem orderItem);

    /**
     * 恢复sku的库存与销量
     *
     * @param skuId
     * @param num
     */
    @Update("update tb_sku set num=num+#{num},sale_num=sale_num-#{num} where id=#{skuId}")
    void resumeSkuStockCount(String skuId, Integer num);
}
