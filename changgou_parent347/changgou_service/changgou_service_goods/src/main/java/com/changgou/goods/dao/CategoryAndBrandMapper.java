package com.changgou.goods.dao;

import com.changgou.pojo.CategoryAndBrand;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/25
 * @description ：
 * @version: 1.0
 */
public interface CategoryAndBrandMapper  extends Mapper<CategoryAndBrand> {
}
