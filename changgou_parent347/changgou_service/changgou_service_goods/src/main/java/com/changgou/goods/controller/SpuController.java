package com.changgou.goods.controller;

import com.changgou.entity.PageResult;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.goods.service.SpuService;
import com.changgou.pojo.Goods;
import com.changgou.pojo.Spu;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/spu")
public class SpuController {


    @Autowired
    private SpuService spuService;

    /**
     * 查询全部数据
     *
     * @return
     */
    @GetMapping
    public Result findAll() {
        List<Spu> spuList = spuService.findAll();
        return new Result(true, StatusCode.OK, "查询成功", spuList);
    }

    /***
     * 根据ID查询数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result findById(@PathVariable("id") String id) {
        Spu spu = spuService.findById(id);
        return new Result(true, StatusCode.OK, "查询成功", spu);
    }

    /**
     * 改造为返回Spu对象,方便SpuFeign调用获取商品详情数据
     * 根据商品id获取商品对象,为生成静态页Feign调用提供服务
     *
     * @param id
     * @return
     */
    @GetMapping("/spuid/{id}")
    public Spu findByspuId(@PathVariable("id") String id) {
        Spu spu = spuService.findById(id);
        return spu;
    }

    /**
     * 根据商品信息Id查询
     *
     * @param id
     * @return
     */
    @GetMapping("/findById/{id}")
    public Result findGoodsBySpuId(@PathVariable String id) {
        Goods goods = spuService.findGoodsBySpuId(id);
        return new Result(true, StatusCode.OK, "查询成功", goods);
    }

    /***
     * 新增数据
     * @param spu
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Spu spu) {
        spuService.add(spu);
        return new Result(true, StatusCode.OK, "添加成功");
    }

    /**
     * 添加商品并维护与分类余品牌的关系
     *
     * @param goods
     * @return
     */
    @PostMapping(value = "/add")
    public Result addGoods(@RequestBody Goods goods) {
        spuService.addGoods(goods);
        return new Result(true, StatusCode.OK, "添加成功");
    }


    /***
     * 修改数据
     * @param spu
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result update(@RequestBody Spu spu, @PathVariable String id) {
        spu.setId(id);
        spuService.update(spu);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 根据商品Id修改
     *
     * @param goods
     * @param id
     * @return
     */
    @PutMapping(value = "/update/{id}")
    public Result updateGoods(@RequestBody Goods goods, @PathVariable String id) {
        goods.getSpu().setId(id);
        spuService.updateGoods(goods);
        return new Result(true, StatusCode.OK, "修改成功");
    }


    /***
     * 根据ID删除品牌数据
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result realDelete(@PathVariable String id) {
        spuService.realDelete(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /***
     * 多条件搜索品牌数据
     * @param searchMap
     * @return
     */
    @GetMapping(value = "/search")
    public Result findList(@RequestParam Map searchMap) {
        List<Spu> list = spuService.findList(searchMap);
        return new Result(true, StatusCode.OK, "查询成功", list);
    }


    /***
     * 分页搜索实现
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result findPage(@RequestParam Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Spu> pageList = spuService.findPage(searchMap, page, size);
        PageResult pageResult = new PageResult(pageList.getTotal(), pageList.getResult());
        return new Result(true, StatusCode.OK, "查询成功", pageResult);
    }

    @PutMapping(value = "/audit/{id}")
    public Result audit(@PathVariable("id") String spuId) {
        spuService.audit(spuId);
        return new Result(true, StatusCode.OK, "审核成功");
    }

    /**
     * 商品下架
     *
     * @param spuId
     * @return
     */
    @PutMapping(value = "/pull/{spuId}")
    public Result pull(@PathVariable String spuId) {
        spuService.pull(spuId);
        return new Result(true, StatusCode.OK, "下架成功");
    }

    /**
     * 商品上架
     *
     * @param spuId
     * @return
     */
    @PutMapping(value = "/put/{id}")
    public Result put(@PathVariable("id") String spuId) {
        spuService.put(spuId);
        return new Result(true, StatusCode.OK, "上架成功");
    }

    /**
     * 根据商品id进行逻辑删除
     *
     * @param spuId
     * @return
     */
    @DeleteMapping(value = "/delete/{id}")
    public Result delete(@PathVariable("id") String spuId) {
        spuService.delete(spuId);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /**
     * 对逻辑删除的商品进行恢复
     *
     * @param spuId
     * @return
     */
    @PutMapping("/restore/{id}")
    public Result restore(@PathVariable("id") String spuId) {
        spuService.restore(spuId);
        return new Result(true, StatusCode.OK, "恢复商品数据成功");
    }
}
