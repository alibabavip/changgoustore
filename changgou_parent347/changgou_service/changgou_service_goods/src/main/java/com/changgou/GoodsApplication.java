package com.changgou;

import com.changgou.util.IdWorker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = {"com.changgou.goods.dao"})
public class GoodsApplication {
    /**
     * 从配置文件中获取工作机器号
     */
    @Value("${workerId}")
    private Integer workerId;

    /**
     * 从配置文件中获取生成随机数的序列号
     */
    @Value("${datacenterId}")
    private Integer datacenterId;

    public static void main(String[] args) {
        SpringApplication.run(GoodsApplication.class);
    }

    /**
     * 初始化分布式ID算法, 将id生成器交给spring管理
     *
     * @return
     */
    @Bean
    public IdWorker initIdWorker() {
        return new IdWorker(workerId, datacenterId);
    }
}
