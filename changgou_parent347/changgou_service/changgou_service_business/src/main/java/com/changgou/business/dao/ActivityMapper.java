package com.changgou.business.dao;

import com.changgou.pojo.Activity;
import tk.mybatis.mapper.common.Mapper;
/**
 * @author liJingJie
 */
public interface ActivityMapper extends Mapper<Activity> {

}
