package com.changgou.business.listener;

import okhttp3.*;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/29
 * @description ：
 * @version: 1.0
 */
@Component
@RabbitListener(queues = "ad_update_queue")
public class ADListener {
    @RabbitHandler
    public void getMessage(String message) {
        //定义调用nginx中lua脚本的url地址
        String url = "http://192.168.200.128/ad_update?position=" + message;
        //创建okHttpClient对象
        OkHttpClient httpClient = new OkHttpClient();
        //创建请求对象
        Request request = new Request.Builder().url(url).build();
        //使用okHrrpClient发送请求
        Call call = httpClient.newCall(request);
        //返回响应,根据响应回调后续业务
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("=======发送请求调用lua脚本失败==========");
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                System.out.println("========调用lua脚本成功========" + response.message());
            }
        });
    }
}
