package com.changgou.pay.service;

import java.util.Map;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/10
 * @version: 1.0
 */
public interface WXPayService {
    /**
     * 生成微信支付二维码
     *
     * @param orderId
     * @param payMoney
     * @return
     */
    Map nativePay(String orderId, Integer payMoney);

    /**
     * 基于微信查询订单信息
     *
     * @param out_trade_no
     * @return
     */
    Map queryOrder(String out_trade_no);

    /**
     * 关闭微信订单
     *
     * @param orderId
     * @return
     */
    Map closeOrder(String orderId);
}
