package com.changgou.pay.service.impl;

import com.changgou.pay.service.WXPayService;
import com.github.wxpay.sdk.WXPay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/10
 * @version: 1.0
 */
@Service
public class WXPayServiceImpl implements WXPayService {
    @Autowired
    private WXPay wxPay;

    @Value("${wxpay.notify_url}")
    private String notify_url;

    /**
     * 统一下单调用,生成微信支付二维码
     *
     * @param orderId
     * @param money
     * @return
     */
    @Override
    public Map nativePay(String orderId, Integer money) {
        try {

            Map<String, String> map = new HashMap<>();

            //封装封装请求参数,商品描述
            map.put("body", "畅购商城");
            //订单号
            map.put("out_trade_no", orderId);
            //map.put("total_fee",String.valueOf(money*100));//金额,以分为单位
            BigDecimal payMoney = new BigDecimal("0.01");
            //以分为单位的金额变成1.00元
            BigDecimal fen = payMoney.multiply(new BigDecimal("100"));
            //去掉后边俩0并向上取整
            fen = fen.setScale(0, BigDecimal.ROUND_UP);
            map.put("total_fee", String.valueOf(fen));
            //终端ip
            map.put("spbill_create_ip", "127.0.0.1");

            //回调通知地址
            map.put("notify_url", notify_url);
            //交易类型: NATIVE,生成二维码
            map.put("trade_type", "NATIVE");

            Map<String, String> payResult = wxPay.unifiedOrder(map);
            return payResult;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 基于微信查询订单信息
     *
     * @param out_trade_no
     * @return
     */
    @Override
    public Map queryOrder(String out_trade_no) {
        try {
            Map map = new HashMap<>();
            map.put("out_trade_no", out_trade_no);
            return wxPay.orderQuery(map);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 关闭微信订单
     *
     * @param orderId
     * @return
     */
    @Override
    public Map closeOrder(String orderId) {
        try {
            Map map = new HashMap();
            map.put("out_trade_no", orderId);
            return wxPay.closeOrder(map);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
