package com.changgou.pay.controller;

import com.alibaba.fastjson.JSON;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.pay.service.WXPayService;
import com.changgou.util.ConvertUtils;
import com.github.wxpay.sdk.WXPayUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/10
 * @description ：
 * @version: 1.0
 */
@RestController
@RequestMapping("/wxpay")
public class WXPayController {
    @Autowired
    private WXPayService wxPayService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 调用支付功能 生成微信支付二维码
     *
     * @param orderId
     * @param payMoney
     * @return
     */
    @GetMapping("/nativePay")
    public Result nativePay(@RequestParam("orderId") String orderId, @RequestParam("money") Integer payMoney) {
        Map map = wxPayService.nativePay(orderId, payMoney);
        return new Result(true, StatusCode.OK, "操作成功", map);
    }

    /**
     * 回调通知
     *
     * @param request
     * @param response
     */
    @RequestMapping("/notify")
    public void notifyLogic(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("支付成功回调。。。。");
        //接收微信返回的结果通知
        String weixinResult = ConvertUtils.convertToString(request.getInputStream());
        System.out.println(weixinResult);

        //将xml转换成map
        Map<String, String> map = WXPayUtil.xmlToMap(weixinResult);
        if ("SUCCESS".equals(map.get("result_code"))) {
            //查询订单信息
            Map orderResult = wxPayService.queryOrder(map.get("out_trade_no"));

            //result_code 微信的返回值,成功或失败
            if (orderResult != null && "SUCCESS".equals(orderResult.get("result_code"))) {

                //向消息队列发送消息
                Map info = new HashMap<>();

                //out_trade_no 订单号
                info.put("orderId", orderResult.get("out_trade_no"));

                //transactionId 微信的交易单号
                info.put("transactionId", orderResult.get("transactionId"));
                rabbitTemplate.convertAndSend("", "order_pay", JSON.toJSONString(info));
                //发送消息,通知客户端
                rabbitTemplate.convertAndSend("paynotify", "", orderResult.get("out_trade_no"));
                //通知微信,已经成功接收通知信息
                response.setContentType("text/xml");
                String value = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
                response.getWriter().write(value);
            } else {
                //打印错误信息描述
                System.out.println(map.get("err_code_des"));
            }
        }
    }

    /**
     * 关闭微信订单
     *
     * @param orderId
     * @return
     */
    @PutMapping("/close/{orderId}")
    public Result<Map> closeOrder(@PathVariable("orderId") String orderId) {
        Map map = wxPayService.closeOrder(orderId);
        return new Result(true, StatusCode.OK, "关闭订单", map);
    }

    /**
     * 查询微信订单
     *
     * @param orderId
     * @return
     */
    @GetMapping("/query/{orderId}")
    public Result<Map> queryOrder(@PathVariable("orderId") String orderId) {
        Map map = wxPayService.queryOrder(orderId);
        return new Result(true, StatusCode.OK, "查询订单", map);
    }
}
