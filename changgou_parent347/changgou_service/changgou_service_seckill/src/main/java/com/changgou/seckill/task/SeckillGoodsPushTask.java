package com.changgou.seckill.task;

import com.changgou.seckill.dao.SeckillGoodsMapper;
import com.changgou.seckill.pojo.SeckillGoods;
import com.changgou.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/12
 * @description ：秒杀定时任务和缓存预热
 * @version: 1.0
 */
@Component
public class SeckillGoodsPushTask {
    @Autowired
    private SeckillGoodsMapper seckillGoodsMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    private static final String SECKILL_GOODS_KEY = "seckill_goods_";
    private static final String SECKILL_GOODS_STOCK_COUNT_KEY="seckill_goods_stock_count_";

    @Scheduled(cron = "0/20 * * * * ?")
    public void seckillGoodsPushToRedis() {
        //1.获取秒杀时间段菜单信息
        List<Date> dateMenus = DateUtil.getDateMenus();

        //2.遍历每一个时间段
        for (Date dateMenu : dateMenus) {
            //将当前时间转换为String,作为redis中的key
            String redisExtName = DateUtil.date2Str(dateMenu);
            //查询商品信息（状态为1，库存大于0，秒杀商品开始时间大于当前时间段，秒杀商品结束时间小于当前时间段，当前商品的id不在redis中）
            Example example = new Example(SeckillGoods.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("status", 1);
            criteria.andGreaterThan("stockCount", 0);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            criteria.andGreaterThanOrEqualTo("startTime", simpleDateFormat.format(dateMenu));

            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            criteria.andLessThan("endTime", simpleDateFormat1.format(DateUtil.addDateHour(dateMenu, 2)));

            //当前商品的id不在redis中
            //keys -> 商品id的集合
            Set keys = redisTemplate.boundHashOps(SECKILL_GOODS_KEY + redisExtName).keys();
            if (keys != null && keys.size() > 0) {
                criteria.andNotIn("id", keys);
            }

            List<SeckillGoods> seckillGoodsList = seckillGoodsMapper.selectByExample(example);

            //存入redis
            for (SeckillGoods seckillGoods : seckillGoodsList) {
                redisTemplate.boundHashOps(SECKILL_GOODS_KEY + redisExtName).put(seckillGoods.getId(), seckillGoods);

                //预加载库存信息

                Integer stockCount = seckillGoods.getStockCount();
                redisTemplate.boundValueOps(SECKILL_GOODS_STOCK_COUNT_KEY+seckillGoods.getId()).set(stockCount);
            }
        }
    }

}
