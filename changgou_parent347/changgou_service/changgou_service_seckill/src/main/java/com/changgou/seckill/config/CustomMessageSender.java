package com.changgou.seckill.config;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/14
 * @description ：自定义消息发送类
 * @version: 1.0
 */
@Component
public class CustomMessageSender implements RabbitTemplate.ConfirmCallback {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RedisTemplate redisTemplate;
    private static final String MESSAGE_CONFIRM_ = "message_confirm_";

    public CustomMessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setConfirmCallback(this);
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack) {
            //发送消息成功,并且该条消息已经完成了持久化
            redisTemplate.delete(correlationData.getId());
            redisTemplate.delete(MESSAGE_CONFIRM_ + correlationData.getId());
        } else {
            //发送消息失败,重发
            Map<String, String> entries = redisTemplate.opsForHash().entries(MESSAGE_CONFIRM_ + correlationData.getId());
            String exchange = entries.get("exchange");
            String routingKey = entries.get("routingKey");
            String sendMessage = entries.get("sendMessage");
            rabbitTemplate.convertAndSend(exchange, routingKey, sendMessage);
        }
    }

    /**
     * 自定义发送方法
     *
     * @param exchange
     * @param routingKey
     * @param message
     */
    public void sendMessage(String exchange, String routingKey, String message) {

        //保存消息内容
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        redisTemplate.opsForValue().set(correlationData.getId(), message);

        //保存该消息的发送元数据(交换机,路由key,message)
        Map<String, String> map = new HashMap<>();
        map.put("exchange", exchange);
        map.put("routingKey", routingKey);
        map.put("sendMessage", message);
        redisTemplate.opsForHash().putAll(MESSAGE_CONFIRM_ + correlationData.getId(), map);

        rabbitTemplate.convertAndSend(exchange, routingKey, message, correlationData);
    }
}
