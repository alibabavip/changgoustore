package com.changgou.seckill.service;

/**
 * @author LJJ
 */
public interface SecKillOrderService {
    /**
     * 秒杀下单
     *
     * @param time
     * @param id
     * @param username
     * @return
     */
    boolean add(String time, Long id, String username);
}
