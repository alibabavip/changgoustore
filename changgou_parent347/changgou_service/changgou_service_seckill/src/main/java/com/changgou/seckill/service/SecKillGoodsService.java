package com.changgou.seckill.service;

import com.changgou.seckill.pojo.SeckillGoods;

import java.util.List;

/**
 * @author LJJ
 */
public interface SecKillGoodsService {
    /**
     * 查询该时间段所有秒杀商品列表
     *
     * @param time
     * @return
     */
    List<SeckillGoods> list(String time);
}
