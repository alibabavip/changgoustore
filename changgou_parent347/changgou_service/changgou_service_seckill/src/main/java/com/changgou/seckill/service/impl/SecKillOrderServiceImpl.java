package com.changgou.seckill.service.impl;

import com.alibaba.fastjson.JSON;
import com.changgou.seckill.config.CustomMessageSender;
import com.changgou.seckill.config.RabbitMQConfig;
import com.changgou.seckill.dao.SeckillOrderMapper;
import com.changgou.seckill.pojo.SeckillGoods;
import com.changgou.seckill.pojo.SeckillOrder;
import com.changgou.seckill.service.SecKillOrderService;
import com.changgou.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/14
 * @version: 1.0
 */
@Service
public class SecKillOrderServiceImpl implements SecKillOrderService {
    private static final String SECKILL_GOODS_KEY = "seckill_goods_";
    private static final String SECKILL_GOODS_STOCK_COUNT_KEY = "seckill_goods_stock_count_";

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private CustomMessageSender customMessageSender;

    @Autowired
    private SeckillOrderMapper seckillOrderMapper;

    /**
     * 秒杀下单
     *
     * @param time     时间段
     * @param id       商品id
     * @param username 登陆人姓名
     * @return
     */
    @Override
    public boolean add(String time, Long id, String username) {

        //防止用户恶意刷单
        String repeatCommitResult = this.preventRepeatCommit(username, id);
        if ("fail".equals(repeatCommitResult)) {
            return false;
        }

        //防止重复相同的商品下单(DB为准)
        SeckillOrder seckillOrder = seckillOrderMapper.selectByUserIdAndGoodsId(username, id);
        if (seckillOrder != null) {
            return false;
        }

        //获取redis中的商品信息
        SeckillGoods seckillGoods = (SeckillGoods) redisTemplate.boundHashOps(SECKILL_GOODS_KEY + time).get(id);
        if (seckillGoods == null) {
            return false;
        }

        //获取redis中的库存信息
        String value = (String) redisTemplate.opsForValue().get(SECKILL_GOODS_STOCK_COUNT_KEY + seckillGoods.getId());
        int redisStockCount = Integer.parseInt(value);

        if (redisStockCount <= 0) {
            return false;
        }

        //预扣减库存,如果扣完库存量<0,删除商品数据与库存数据(原子性操作)
        //两种实现方式 1.redis的decrement increment递增或递减1  2)要实现的业务写在lua脚本里
        Long decrement = redisTemplate.opsForValue().decrement(SECKILL_GOODS_STOCK_COUNT_KEY + seckillGoods.getId());
        if (decrement < 0) {
            //库存没了,删除商品信息
            redisTemplate.boundHashOps(SECKILL_GOODS_KEY + time).delete(id);

            //删除库存信息
            redisTemplate.delete(SECKILL_GOODS_STOCK_COUNT_KEY + id);
            return false;
        }

        //创建订单信息
        seckillOrder = new SeckillOrder();
        seckillOrder.setId(idWorker.nextId());
        seckillOrder.setSeckillId(id);
        seckillOrder.setMoney(seckillGoods.getCostPrice());
        seckillOrder.setUserId(username);
        seckillOrder.setSellerId(seckillGoods.getSellerId());
        seckillOrder.setCreateTime(new Date());
        seckillOrder.setStatus("0");

        //发送消息(消息必达)
        customMessageSender.sendMessage("", RabbitMQConfig.SECKILL_ORDER, JSON.toJSONString(seckillOrder));
        return true;
    }

    /**
     * 防止用户恶意刷单
     *
     * @param username
     * @param id
     * @return
     */
    private String preventRepeatCommit(String username, Long id) {
        String redisKey = "seckill_user_" + username + "_id_" + id;
        //步长
        Long increment = redisTemplate.opsForValue().increment(redisKey, 1);
        if (increment == 1) {
            //设置过期时间
            redisTemplate.expire(redisKey, 5, TimeUnit.MINUTES);
            return "success";
        }
        return "fail";
    }
}
