package com.changgou.seckill.dao;

import com.changgou.seckill.pojo.SeckillGoods;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/12
 * @version: 1.0
 */
public interface SeckillGoodsMapper  extends Mapper<SeckillGoods> {
}
