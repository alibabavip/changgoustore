package com.changgou.seckill.service.impl;

import com.changgou.seckill.pojo.SeckillGoods;
import com.changgou.seckill.service.SecKillGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/13
 * @version: 1.0
 */
@Service
public class SecKillGoodsServiceImpl implements SecKillGoodsService {
    @Autowired
    private RedisTemplate redisTemplate;
    private static final String SECKILL_GOODS_KEY = "seckill_goods_";
    private static final String SECKILL_GOODS_STOCK_COUNT_KEY = "seckill_goods_stock_count_";

    @Override
    public List<SeckillGoods> list(String time) {

        List<SeckillGoods> secKillGoodsList = redisTemplate.boundHashOps(SECKILL_GOODS_KEY + time).values();

        //更新商品库存信息来源
        for (SeckillGoods seckillGoods : secKillGoodsList) {
            String value = (String) redisTemplate.opsForValue().get(SECKILL_GOODS_STOCK_COUNT_KEY + seckillGoods.getId());
            seckillGoods.setStockCount(Integer.parseInt(value));
        }
        return secKillGoodsList;
    }
}
