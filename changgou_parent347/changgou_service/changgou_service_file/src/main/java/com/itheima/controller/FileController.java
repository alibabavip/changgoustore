package com.itheima.controller;

import com.itheima.util.FastDFSClient;
import com.itheima.util.FastDFSFile;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/7/24
 * @description ：
 * @version: 1.0
 */
@RestController
@RequestMapping("/file")
public class FileController {
    /**
     * 上传文件
     *
     * @param file 这个名称和页面上的上传域的name属性值对应
     * @return 返回上传后文件存储的URL地址
     */
    @PostMapping("/upload")
    public String uploadFile(MultipartFile file) {
        try {
            if (file != null) {
                //获取文件的完整名称
                String fileName = file.getOriginalFilename();
                String ext = "";
                // 根据文件名称获取扩展名
                if (!StringUtils.isEmpty(fileName)) {
                    ext = fileName.substring(fileName.lastIndexOf(".") + 1);
                }
                //获取文件内容
                byte[] contet = file.getBytes();
                //封装上传的文件对象
                FastDFSFile fastDFSFile = new FastDFSFile(fileName, contet, ext);
                //5上传并返回存储后的路径
                String[] path = FastDFSClient.upload(fastDFSFile);
                //封装上传后的完整URL地址返回
                String url = FastDFSClient.getTrackerUrl() + path[0] + "/" + path[1];
                return url;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("======上传文件出错!=======");
        }
        return null;
    }
}
