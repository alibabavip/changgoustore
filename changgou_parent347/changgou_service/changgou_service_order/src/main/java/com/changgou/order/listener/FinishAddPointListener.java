package com.changgou.order.listener;

import com.alibaba.fastjson.JSON;
import com.changgou.order.config.RabbitMQConfig;
import com.changgou.order.pojo.Task;
import com.changgou.order.service.TaskService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/10
 * @description ：监听订单任务并删除原任务信息
 * @version: 1.0
 */
@Component
public class FinishAddPointListener {
    @Autowired
    private TaskService taskService;

    @RabbitListener(queues = RabbitMQConfig.CG_BUYING_FINISHADDPOINT)
    public void reveiveFinishAddPoint(String message) {
        System.out.println("订单服务：接收完成修改积分消息");
        Task task = JSON.parseObject(message, Task.class);

        //增加历史任务信息,删除原任务信息
        taskService.delTask(task);
    }
}
