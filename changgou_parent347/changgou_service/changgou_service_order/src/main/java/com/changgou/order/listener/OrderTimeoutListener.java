package com.changgou.order.listener;

import com.changgou.order.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/11
 * @description ：订单超时监听类
 * @version: 1.0
 */
@Component
public class OrderTimeoutListener {
    @Autowired
    private OrderService orderService;

    @RabbitListener(queues = "queue.ordertimeout")
    public void receiveOrderTimeOutMessage(String orderId) {
        System.out.println("触发关闭订单业务:" + orderId);
        orderService.closeOrder(orderId);
    }
}
