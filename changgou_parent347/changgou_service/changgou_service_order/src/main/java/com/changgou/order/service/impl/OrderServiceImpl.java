package com.changgou.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.order.config.RabbitMQConfig;
import com.changgou.order.dao.*;
import com.changgou.order.pojo.*;
import com.changgou.order.service.CartService;
import com.changgou.order.service.OrderService;
import com.changgou.pay.feign.PayFeign;
import com.changgou.user.feign.UserFeign;
import com.changgou.util.IdWorker;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private SkuFeign skuFeign;
    @Autowired
    private CartService cartService;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserFeign userFeign;
    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private OrderLogMapper orderLogMapper;
    @Autowired
    private PayFeign payFeign;
    @Autowired
    private OrderItemMapper orderItemMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private OrderConfigMapper orderConfigMapper;

    /**
     * 查询全部列表
     *
     * @return
     */
    @Override
    public List<Order> findAll() {
        return orderMapper.selectAll();
    }

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    @Override
    public Order findById(String id) {
        return orderMapper.selectByPrimaryKey(id);
    }


    /**
     * 增加顶单
     *
     * @param order
     */
    @Override
    //@GlobalTransactional(name="order-add",timeoutMills = 100000)
    @Transactional
    public String add(Order order) {
        // 获取购物车中的数据 List <OrderItem>  cartList =  redisTemplate.boundHashOps("Cart_" + order.getUsername()).values();
        Map cartMap = cartService.list(order.getUsername());
        List<OrderItem> orderItemList = (List<OrderItem>) cartMap.get("orderItemList");
        Integer totalNum = (Integer) cartMap.get("totalNum");
        Integer totalMoney = (Integer) cartMap.get("totalMoney");

        //生成订单
        order.setTotalMoney(totalMoney);
        order.setTotalNum(totalNum);
        order.setPayMoney((Integer) cartMap.get("totalMoney"));
        order.setCreateTime(new Date());
        order.setUpdateTime(order.getCreateTime());
        //0:未评价，1：已评价
        order.setBuyerRate("0");
        //来源，1：WEB
        order.setSourceType("1");
        //0:未完成,1:已完成，2：已退货
        order.setOrderStatus("0");
        //0:未支付，1：已支付，2：支付失败
        order.setPayStatus("0");
        //0:未发货，1：已发货，2：已收货
        order.setConsignStatus("0");
        String orderId = idWorker.nextId() + "";
        order.setId(orderId);
        int count = orderMapper.insertSelective(order);

        //生成订单项
        for (OrderItem orderItem : orderItemList) {
            orderItem.setId(idWorker.nextId() + "");
            //是否退货
            orderItem.setIsReturn("0");
            orderItem.setOrderId(order.getId());
            orderItemMapper.insertSelective(orderItem);
        }


        //减库存
        skuFeign.decrCount(order.getUsername());
        System.out.println("生成新任务");
        /**
         * 生成订单任务并发送到消息队列
         */
        Task task = new Task();
        task.setCreateTime(new Date());
        task.setUpdateTime(new Date());
        //设置任务到交换机
        task.setMqExchange(RabbitMQConfig.EX_BUYING_ADDPOINTURSE);
        //设置路由键
        task.setMqRoutingkey(RabbitMQConfig.CG_BUYING_ADDPOINT_KEY);

        Map info = new HashMap();
        info.put("username", order.getUsername());
        info.put("orderId", order.getId());
        info.put("point", order.getPayMoney());
        task.setRequestBody(JSON.toJSONString(info));
        taskMapper.insertSelective(task);
        System.out.println("生成新任务成功");
        //增加积分
        //userFeign.addPoints(10);
        //清空购物车
        redisTemplate.delete("Cart_" + order.getUsername());

        //发送延迟消息
        rabbitTemplate.convertAndSend("", "queue.ordercreate", orderId);
        return orderId;
    }


    /**
     * 修改
     *
     * @param order
     */
    @Override
    public void update(Order order) {
        orderMapper.updateByPrimaryKey(order);
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void delete(String id) {
        orderMapper.deleteByPrimaryKey(id);
    }


    /**
     * 条件查询
     *
     * @param searchMap
     * @return
     */
    @Override
    public List<Order> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return orderMapper.selectByExample(example);
    }

    /**
     * 分页查询
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public Page<Order> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        return (Page<Order>) orderMapper.selectAll();
    }

    /**
     * 条件+分页查询
     *
     * @param searchMap 查询条件
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @Override
    public Page<Order> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page, size);
        Example example = createExample(searchMap);
        return (Page<Order>) orderMapper.selectByExample(example);
    }

    /**
     * 构建查询对象
     *
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap) {
        Example example = new Example(Order.class);
        Example.Criteria criteria = example.createCriteria();
        if (searchMap != null) {
            // 订单id
            if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                criteria.andEqualTo("id", searchMap.get("id"));
            }
            // 支付类型，1、在线支付、0 货到付款
            if (searchMap.get("payType") != null && !"".equals(searchMap.get("payType"))) {
                criteria.andEqualTo("payType", searchMap.get("payType"));
            }
            // 物流名称
            if (searchMap.get("shippingName") != null && !"".equals(searchMap.get("shippingName"))) {
                criteria.andLike("shippingName", "%" + searchMap.get("shippingName") + "%");
            }
            // 物流单号
            if (searchMap.get("shippingCode") != null && !"".equals(searchMap.get("shippingCode"))) {
                criteria.andLike("shippingCode", "%" + searchMap.get("shippingCode") + "%");
            }
            // 用户名称
            if (searchMap.get("username") != null && !"".equals(searchMap.get("username"))) {
                criteria.andLike("username", "%" + searchMap.get("username") + "%");
            }
            // 买家留言
            if (searchMap.get("buyerMessage") != null && !"".equals(searchMap.get("buyerMessage"))) {
                criteria.andLike("buyerMessage", "%" + searchMap.get("buyerMessage") + "%");
            }
            // 是否评价
            if (searchMap.get("buyerRate") != null && !"".equals(searchMap.get("buyerRate"))) {
                criteria.andLike("buyerRate", "%" + searchMap.get("buyerRate") + "%");
            }
            // 收货人
            if (searchMap.get("receiverContact") != null && !"".equals(searchMap.get("receiverContact"))) {
                criteria.andLike("receiverContact", "%" + searchMap.get("receiverContact") + "%");
            }
            // 收货人手机
            if (searchMap.get("receiverMobile") != null && !"".equals(searchMap.get("receiverMobile"))) {
                criteria.andLike("receiverMobile", "%" + searchMap.get("receiverMobile") + "%");
            }
            // 收货人地址
            if (searchMap.get("receiverAddress") != null && !"".equals(searchMap.get("receiverAddress"))) {
                criteria.andLike("receiverAddress", "%" + searchMap.get("receiverAddress") + "%");
            }
            // 订单来源：1:web，2：app，3：微信公众号，4：微信小程序  5 H5手机页面
            if (searchMap.get("sourceType") != null && !"".equals(searchMap.get("sourceType"))) {
                criteria.andEqualTo("sourceType", searchMap.get("sourceType"));
            }
            // 交易流水号
            if (searchMap.get("transactionId") != null && !"".equals(searchMap.get("transactionId"))) {
                criteria.andLike("transactionId", "%" + searchMap.get("transactionId") + "%");
            }
            // 订单状态
            if (searchMap.get("orderStatus") != null && !"".equals(searchMap.get("orderStatus"))) {
                criteria.andEqualTo("orderStatus", searchMap.get("orderStatus"));
            }
            // 支付状态
            if (searchMap.get("payStatus") != null && !"".equals(searchMap.get("payStatus"))) {
                criteria.andEqualTo("payStatus", searchMap.get("payStatus"));
            }
            // 发货状态
            if (searchMap.get("consignStatus") != null && !"".equals(searchMap.get("consignStatus"))) {
                criteria.andEqualTo("consignStatus", searchMap.get("consignStatus"));
            }
            // 是否删除
            if (searchMap.get("isDelete") != null && !"".equals(searchMap.get("isDelete"))) {
                criteria.andEqualTo("isDelete", searchMap.get("isDelete"));
            }

            // 数量合计
            if (searchMap.get("totalNum") != null) {
                criteria.andEqualTo("totalNum", searchMap.get("totalNum"));
            }
            // 金额合计
            if (searchMap.get("totalMoney") != null) {
                criteria.andEqualTo("totalMoney", searchMap.get("totalMoney"));
            }
            // 优惠金额
            if (searchMap.get("preMoney") != null) {
                criteria.andEqualTo("preMoney", searchMap.get("preMoney"));
            }
            // 邮费
            if (searchMap.get("postFee") != null) {
                criteria.andEqualTo("postFee", searchMap.get("postFee"));
            }
            // 实付金额
            if (searchMap.get("payMoney") != null) {
                criteria.andEqualTo("payMoney", searchMap.get("payMoney"));
            }

        }
        return example;
    }

    /**
     * 更新订单支付状态
     *
     * @param orderId
     * @param transactionId
     */
    @Override
    public void updateOrderPayStatus(String orderId, String transactionId) {
        //查询订单信息
        Order order = orderMapper.selectByPrimaryKey(orderId);
        //存在订单且状态为0
        if (order != null && "0".equals(order.getPayStatus())) {
            order.setPayStatus("1");
            order.setOrderStatus("1");
            order.setUpdateTime(new Date());
            order.setPayTime(new Date());
            //微信返回的交易流水号
            order.setTransactionId(transactionId);
            orderMapper.updateByPrimaryKeySelective(order);

            //记录订单变动日志
            OrderLog orderLog = new OrderLog();
            orderLog.setId(idWorker.nextId() + "");
            // 系统
            orderLog.setOperater("system");
            //当前日期
            orderLog.setOperateTime(new Date());
            orderLog.setOrderStatus("1");
            orderLog.setPayStatus("1");
            orderLog.setRemarks("支付流水号" + transactionId);
            orderLog.setOrderId(order.getId());
            orderLogMapper.insert(orderLog);
        }
    }

    /**
     * 关闭订单
     *
     * @param orderId
     */
    @Override
    @Transactional
    public void closeOrder(String orderId) {
        //1.根据id查询订单信息,判断订单是否存在,订单支付状态是否为未支付
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if (order == null) {
            throw new RuntimeException("当前订单不存在");
        }
        if (!"0".equals(order.getPayStatus())) {
            System.out.println("当前订单无需处理");
            return;
        }
        //2.基于微信查询订单支付状态
        Map map = payFeign.queryOrder(orderId).getData();
        //3.如果微信返回的是success,则修改数据库订单状态
        if ("SUCCESS".equals(map.get("trade_state"))) {
            //数据库补偿
            System.out.println("补偿数据库中订单状态为已支付");
            this.updateOrderPayStatus(orderId, (String) map.get("transaction_id"));
            return;
        }
        //4.如果为未支付,则修改订单,新增日志,恢复库存,关闭订单
        if ("NOTPAY".equals(map.get("trade_state"))) {
            System.out.println("执行关闭！");
            //关闭时间
            order.setCloseTime(new Date());
            //关闭状态
            order.setOrderStatus("4");
            //更新
            orderMapper.updateByPrimaryKeySelective(order);

            //记录订单变动日志
            OrderLog orderLog = new OrderLog();
            orderLog.setId(idWorker.nextId() + "");
            //系统
            orderLog.setOperater("system");
            //当前日期
            orderLog.setOperateTime(new Date());
            orderLog.setOrderStatus("4");
            orderLog.setOrderId(order.getId());
            orderLogMapper.insert(orderLog);

            //库存与销量恢复
            OrderItem _orderItem = new OrderItem();
            _orderItem.setOrderId(orderId);
            List<OrderItem> orderItemList = orderItemMapper.select(_orderItem);

            for (OrderItem orderItem : orderItemList) {
                //skuId num(用户购买量)
                skuFeign.resumeSkuStockCount(orderItem.getSkuId(), orderItem.getNum());
            }

            //关闭微信订单
            payFeign.closeOrder(orderId);
        }
    }

    /**
     * 批量发货
     *
     * @param orders 订单列表
     */
    @Override
    @Transactional
    public void batchSend(List<Order> orders) {
        for (Order order : orders) {
            if (order.getId() == null) {
                throw new RuntimeException("订单不存在");
            }
            //判断物流名称物流编号
            if (order.getShippingName() == null || order.getShippingCode() == null) {
                throw new RuntimeException("物流信息有误");
            }
        }

        for (Order order : orders) {
            Order order1 = orderMapper.selectByPrimaryKey(order.getId());
            if (!"0".equals(order1.getConsignStatus()) || !"1".equals(order1.getOrderStatus())) {
                throw new RuntimeException("订单状态信息有误");
            }
        }

        //发货
        for (Order order : orders) {
            //订单状态 已发货
            order.setOrderStatus("2");
            //发货状态  已发货
            order.setConsignStatus("1");
            //发货时间
            order.setConsignTime(new Date());
            //更新时间
            order.setUpdateTime(new Date());
            orderMapper.updateByPrimaryKeySelective(order);

            //记录订单变动日志
            OrderLog orderLog = new OrderLog();
            orderLog.setId(idWorker.nextId() + "");
            //当前日期
            orderLog.setOperateTime(new Date());
            //系统管理员
            orderLog.setOperater("admin");
            //已完成
            orderLog.setOrderStatus("2");
            //发状态（0未发货 1已发货）
            orderLog.setConsignStatus("1");
            orderLog.setOrderId(order.getId());
            orderLogMapper.insertSelective(orderLog);
        }
    }

    /**
     * 确认收货
     *
     * @param orderId
     * @param operator
     */
    @Override
    public void take(String orderId, String operator) {
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if (order == null) {
            throw new RuntimeException("订单不存在");
        }

        if (!"1".equals(order.getConsignStatus())) {
            throw new RuntimeException("订单未发货");
        }
        //已送达
        order.setConsignStatus("2");
        //已完成
        order.setOrderStatus("3");
        order.setUpdateTime(new Date());
        //交易结束
        order.setEndTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);

        //记录订单变动日志
        OrderLog orderLog = new OrderLog();
        orderLog.setId(idWorker.nextId() + "");
        orderLog.setOperateTime(new Date());
        //系统？管理员？用户？
        orderLog.setOperater(operator);
        orderLog.setOrderStatus("3");
        orderLog.setOrderId(order.getId());
        orderLogMapper.insertSelective(orderLog);
    }

    @Override
    public void autoTack() {
        //从订单配置表中获取订单自动确认期限
        OrderConfig orderConfig = orderConfigMapper.selectByPrimaryKey(1);
        Integer takeTimeout = orderConfig.getTakeTimeout();

        //得到当前日期几天(即自动确认期限),作为过期时间节点
        //当天日期
        LocalDate now = LocalDate.now();
        //当天时间减去15天的日期
        LocalDate localDate = now.plusDays(-takeTimeout);

        //从订单表中获取过期订单（发货时间小于过期时间，且为未确认收货状态）
        Example example = new Example(Order.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLessThan("consignTime", localDate);
        criteria.andEqualTo("orderStatus", 2);
        List<Order> orderList = orderMapper.selectByExample(example);

        if (orderList != null && orderList.size() > 0) {
            for (Order order : orderList) {
                this.take(order.getId(), "system");
            }
        }
    }
}
