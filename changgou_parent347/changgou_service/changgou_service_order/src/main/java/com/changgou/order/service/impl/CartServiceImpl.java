package com.changgou.order.service.impl;

import com.changgou.entity.Result;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.goods.feign.SpuFeign;
import com.changgou.order.service.CartService;
import com.changgou.order.pojo.OrderItem;
import com.changgou.pojo.Sku;
import com.changgou.pojo.Spu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/5
 * @description ：购物车业务实现
 * @version: 1.0
 */
@Service
public class CartServiceImpl implements CartService {
    private static final String CART = "Cart_";
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    private SkuFeign skuFeign;
    @Autowired
    private SpuFeign spuFeign;

    /**
     * 添加购物车
     *
     * @param username 用户名
     * @param skuId    商品Id
     * @param num      商品数量
     */
    @Override
    public void add(String username, String skuId, Integer num) {
        OrderItem orderItem = (OrderItem) redisTemplate.boundHashOps(CART + username).get(skuId);
        if (orderItem != null) {
            //更新数量,总价钱
            orderItem.setNum(orderItem.getNum() + num);
            Integer itemNum = orderItem.getNum();
            if (itemNum <= 0) {
                redisTemplate.boundHashOps(CART + username).delete(skuId);
                return;
            }
            orderItem.setMoney(orderItem.getNum() * orderItem.getPrice());
            orderItem.setPayMoney(orderItem.getNum() * orderItem.getPrice());
        } else {
            //不存在，新增购物车
            Result<Sku> skuResult = skuFeign.findById(skuId);
            Sku sku = skuResult.getData();
            Spu spu = spuFeign.findByspuId(sku.getSpuId());
            //将SKU转换成OrderItem
            orderItem = this.sku2OrderItem(sku, spu, num);
        }
        //存入redis
        redisTemplate.boundHashOps(CART + username).put(skuId, orderItem);
    }

    private OrderItem sku2OrderItem(Sku sku, Spu spu, Integer num) {
        OrderItem orderItem = new OrderItem();
        orderItem.setSpuId(sku.getSpuId());
        orderItem.setSkuId(sku.getId());
        orderItem.setName(sku.getName());
        orderItem.setPrice(sku.getPrice());
        orderItem.setNum(num);
        //单价*数量
        orderItem.setMoney(num * orderItem.getPrice());
        //实付金额
        orderItem.setPayMoney(num * orderItem.getPrice());
        orderItem.setImage(sku.getImage());
        //重量=单个重量*数量
        orderItem.setWeight(sku.getWeight() * num);
        //分类ID设置
        orderItem.setCategoryId1(spu.getCategory1Id());
        orderItem.setCategoryId2(spu.getCategory2Id());
        orderItem.setCategoryId3(spu.getCategory3Id());
        return orderItem;
    }

    @Override
    public Map list(String username) {
        Map map = new HashMap<>();

        List<OrderItem> orderItemList = redisTemplate.boundHashOps(CART + username).values();
        map.put("orderItemList", orderItemList);

        Integer totalNum = 0;
        Integer totalMoney = 0;
        for (OrderItem orderItem : orderItemList) {
            totalNum += orderItem.getNum();
            totalMoney += orderItem.getMoney();
        }
        map.put("totalNum",totalNum);
        map.put("totalMoney",totalMoney);
        return map;
    }
}
