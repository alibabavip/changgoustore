package com.changgou.order.listener;

import com.changgou.order.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/12
 * @description ：自动收货订单监听
 * @version: 1.0
 */
@Component
public class OrderTackListener {
    @Autowired
    private OrderService orderService;
    @RabbitListener(queues = "order_tack")
    public void receiveAutoOrderTack(String message){
        //出发自动收货业务
        orderService.autoTack();
    }
}
