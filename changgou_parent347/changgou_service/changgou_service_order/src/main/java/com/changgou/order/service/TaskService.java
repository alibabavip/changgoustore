package com.changgou.order.service;

import com.changgou.order.pojo.Task;

/**
 * @author LJJ
 */
public interface TaskService {
    /**
     * 删除订单任务
     *
     * @param task
     */
    void delTask(Task task);
}
