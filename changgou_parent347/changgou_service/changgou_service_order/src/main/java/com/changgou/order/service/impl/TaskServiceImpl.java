package com.changgou.order.service.impl;

import com.changgou.order.dao.TaskHisMapper;
import com.changgou.order.dao.TaskMapper;
import com.changgou.order.pojo.Task;
import com.changgou.order.pojo.TaskHis;
import com.changgou.order.service.TaskService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Date;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/10
 * @description ：删除订单任务
 * @version: 1.0
 */
@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private TaskHisMapper taskHisMapper;

    @Override
    @Transactional
    public void delTask( Task task) {
        //设置删除时间
        TaskHis taskHis = new TaskHis();
        Long taskId = task.getId();
        task.setId(null);
        task.setDeleteTime(new Date());

        BeanUtils.copyProperties(task,taskHis);
        //新增taskhis
        taskHisMapper.insertSelective(taskHis);

        //删除原有task
        task.setId(taskId);
        taskMapper.deleteByPrimaryKey(task);

        System.out.println("删除原任务成功");
    }
}
