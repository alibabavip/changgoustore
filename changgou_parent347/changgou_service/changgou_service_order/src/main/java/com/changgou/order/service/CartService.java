package com.changgou.order.service;

import java.util.Map;

/**
 * @author liJingJie
 */
public interface CartService {
    /**
     * 添加购物车
     *
     * @param username
     * @param skuId
     * @param num
     */
    void add(String username, String skuId, Integer num);

    /**
     * 查询用户购物车列表
     *
     * @param username
     * @return
     */
    Map list(String username);
}
