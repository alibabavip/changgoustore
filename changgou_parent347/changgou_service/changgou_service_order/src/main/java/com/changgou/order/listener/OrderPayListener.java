package com.changgou.order.listener;

import com.alibaba.fastjson.JSON;
import com.changgou.order.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/10
 * @description ：监听订单状态
 * @version: 1.0
 */
@Component
public class OrderPayListener {
    @Autowired
    private OrderService orderService;

    @RabbitListener(queues = "order_pay")
    public void receivePayMessage(String message) {
        Map<String, String> map = JSON.parseObject(message, Map.class);

        //调用业务层完成状态修改
        orderService.updateOrderPayStatus(map.get("orderId"),map.get("transactionId"));
    }
}
