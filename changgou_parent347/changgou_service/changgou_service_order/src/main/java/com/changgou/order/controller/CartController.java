package com.changgou.order.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.order.config.TokenDecode;
import com.changgou.order.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/5
 * @description ：购物车
 * @version: 1.0
 */
@RestController
@RequestMapping("/cart")
@CrossOrigin
public class CartController {
    @Autowired
    private CartService cartService;
    @Autowired
    private TokenDecode tokenDecode;

    /**
     * 添加购物车
     *
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/add")
    public Result add(@RequestParam("skuId") String skuId, @RequestParam("num") Integer num) {
        //获取当前登录人CartService 暂时写死，后期改为动态
        //String username = "itcast";
        String username = tokenDecode.getUserInfo().get("username");
        cartService.add(username, skuId, num);
        return new Result(true, StatusCode.OK, "添加购物车成功");
    }

    /***
     * 查询用户购物车列表
     * @return
     */
    @GetMapping("/list")
    public Map list() {
        //暂时静态，后续修改
        //String username = "itcast";
        String username = tokenDecode.getUserInfo().get("username");
        Map map = cartService.list(username);
        return map;
    }
}
