package com.changgou.webOrder.controller;

import com.changgou.entity.Result;
import com.changgou.order.feign.CartFeign;
import com.changgou.order.feign.OrderFeign;
import com.changgou.order.pojo.Order;
import com.changgou.order.pojo.OrderItem;
import com.changgou.user.feign.AddressFeign;
import com.changgou.user.pojo.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/6
 * @version: 1.0
 */
@Controller
@RequestMapping("/worder")
public class OrderController {
    @Autowired
    private AddressFeign addressFeign;
    @Autowired
    private CartFeign cartFeign;
    @Autowired
    private OrderFeign orderFeign;

    @RequestMapping("/ready/order")
    public String toOrder(Model model) {
        //地址列表
        Result<List<Address>> addressResult = addressFeign.list();
        List<Address> addressList = addressResult.getData();
        model.addAttribute("address", addressList);

        //购物信息
        Map cartMap = cartFeign.list();
        List<OrderItem> orderItemList = (List<OrderItem>) cartMap.get("orderItemList");
        Integer totalNum = (Integer) cartMap.get("totalNum");
        Integer totalMoney = (Integer) cartMap.get("totalMoney");
        model.addAttribute("carts", orderItemList);
        model.addAttribute("totalNum", totalNum);
        model.addAttribute("totalMoney", totalMoney);

        //加载默认收件人信息
        for (Address address : addressList) {
            if ("1".equals(address.getIsDefault())) {
                model.addAttribute("defaultAddr", address);
                break;
            }
        }
        return "order";
    }

    /**
     * 添加订单
     *
     * @param order
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add(@RequestBody Order order) {
        return orderFeign.add(order);
    }

    /**
     * 携带着订单Id和金额跳转到支付页面
     *
     * @param orderId
     * @param model
     * @return
     */
    @GetMapping("/topayhtml")
    public String topayhtml(String orderId, Model model) {
        Result<Order> orderResult = orderFeign.findById(orderId);
        Order order = orderResult.getData();

        model.addAttribute("orderId", orderId);
        model.addAttribute("payMoney", order.getPayMoney());
        return "pay";
    }
}
