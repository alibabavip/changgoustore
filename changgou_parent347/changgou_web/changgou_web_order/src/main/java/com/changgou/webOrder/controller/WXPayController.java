package com.changgou.webOrder.controller;

import com.changgou.entity.Result;
import com.changgou.order.feign.OrderFeign;
import com.changgou.order.pojo.Order;
import com.changgou.pay.feign.PayFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/10
 * @description ：
 * @version: 1.0
 */
@Controller
@RequestMapping("/wxpay")
public class WXPayController {
    @Autowired
    private OrderFeign orderFeign;
    @Autowired
    private PayFeign payFeign;

    @GetMapping
    public String wxPay(String orderId, Model model) {
        //判断当前订单是否存在
        Result<Order> orderResult = orderFeign.findById(orderId);
        Order order = orderResult.getData();
        if (order == null) {
            return "fail";
        }

        //判断当前订单是否为已支付
        if ("1".equals(order.getPayStatus())) {
            //已支付
            return "fail";
        }

        //基于feign调用统一下单
        Result result = payFeign.nativePay(orderId, order.getPayMoney());
        Map map = (Map) result.getData();

        //判断返回结果,如果为success
        if ("FALL".equals(map.get("return_code"))) {
            return "fail";
        }

        //携带参数跳转页面
        map.put("orderId", orderId);
        map.put("payMoney", order.getPayMoney());
        model.addAllAttributes(map);
        return "wxpay";
    }

    /**
     * 跳转到支付成功页面
     *
     * @param payMoney
     * @param model
     * @return
     */
    @GetMapping("/topaysuccess")
    public String topaysuccess(Integer payMoney, Model model) {
        model.addAttribute("payMoney", payMoney);
        return "paysuccess";
    }
}
