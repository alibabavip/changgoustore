package com.changgou.webOrder.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.order.feign.CartFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/6
 * @description ：购物车渲染
 * @version: 1.0
 */
@Controller
@RequestMapping("/wcart")
public class CartController {
    @Autowired
    private CartFeign cartFeign;

    /**
     * 查询购物车
     *
     * @param model
     * @return
     */
    @GetMapping("/list")
    public String list(Model model) {
        Map map = cartFeign.list();
        model.addAttribute("items", map);
        return "cart";
    }

    @GetMapping("/add")
    @ResponseBody
    public Result add(String skuId, Integer num) {
        //添加购物车
        cartFeign.add(skuId, num);
        //查询购物车
        Map map = cartFeign.list();
        //返回结果
        return new Result(true, StatusCode.OK, "添加购物车成功", map);
    }
}
