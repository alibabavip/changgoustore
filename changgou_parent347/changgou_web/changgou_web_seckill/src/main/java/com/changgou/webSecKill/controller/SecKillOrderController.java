package com.changgou.webSecKill.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.seckill.feign.SecKillOrderFeign;
import com.changgou.util.RandomUtil;
import com.changgou.webSecKill.aspect.AccessLimit;
import com.changgou.webSecKill.util.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author LJJ
 */
@Controller
@RequestMapping("/wseckillorder")
public class SecKillOrderController {

    @Autowired
    private SecKillOrderFeign secKillOrderFeign;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 秒杀下单
     *
     * @param time
     * @param id
     * @param token
     * @return
     * @AccessLimit 限流
     */
    @RequestMapping("/add")
    @ResponseBody
   // @AccessLimit
    public Result add(String time, Long id, String token) {
        //开始秒杀下单业务
        //基于feign调用后端服务
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Map<String, String> map = CookieUtil.readCookie(request, "uid");
        String jti = map.get("uid");
        //校验密文有效
        String redisToken = (String) redisTemplate.opsForValue().get("interfaceToken" + jti);
        if (StringUtils.isEmpty(redisToken) || !token.equals(redisToken)) {
            return new Result(false, StatusCode.ERROR, "下单失败");
        }
        Result result = secKillOrderFeign.add(time, id);
        return result;
    }

    /**
     * 接口隐藏
     *
     * @return
     */
    @GetMapping("/getToken")
    @ResponseBody
    public String getToken() {
        //获取jti
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Map<String, String> map = CookieUtil.readCookie(request, "uid");
        String jti = map.get("uid");
        String randomString = RandomUtil.getRandomString();
        redisTemplate.opsForValue().set("interfaceToken" + jti, randomString, 20, TimeUnit.SECONDS);

        return randomString;
    }
}
