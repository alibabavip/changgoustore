package com.changgou.webSecKill.aspect;

import java.lang.annotation.*;

/**
 * @author LJJ
 */
@Inherited
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessLimit {
}
