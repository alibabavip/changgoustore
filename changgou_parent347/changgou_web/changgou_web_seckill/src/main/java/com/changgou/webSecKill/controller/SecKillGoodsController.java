package com.changgou.webSecKill.controller;

import com.changgou.entity.Result;
import com.changgou.seckill.feign.SecKillGoodsFeign;
import com.changgou.seckill.pojo.SeckillGoods;
import com.changgou.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/13
 * @version: 1.0
 */
@Controller
@CrossOrigin
@RequestMapping("/wseckillgoods")
public class SecKillGoodsController {
    @Autowired
    private SecKillGoodsFeign secKillGoodsFeign;

    @GetMapping("/toIndex")
    public String toIndex() {
        return "seckill-index";
    }

    /**
     * 获取时间段信息
     *
     * @return
     */
    @GetMapping("/timeMenus")
    @ResponseBody
    public List<String> timeMenus() {
        List<String> dateList = new ArrayList<>();

        //获取根据当前时间的五个时间段
        List<Date> dateMenus = DateUtil.getDateMenus();
        for (Date dateMenu : dateMenus) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateList.add(simpleDateFormat.format(dateMenu));
        }
        return dateList;
    }

    /**
     * 按照时间点查询商品信息
     *
     * @param time
     * @return
     */
    @GetMapping("/list")
    @ResponseBody
    public Result list(String time) {

        Result<List<SeckillGoods>> result = secKillGoodsFeign.list(DateUtil.formatStr(time));
        return result;
    }
}
