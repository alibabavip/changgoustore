package com.changgou.task;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author ：LJJ
 * @date ：Created in 2019/8/12
 * @description ：定时任务类
 * @version: 1.0
 */
@Component
public class OrderTask {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Scheduled(cron = "0 0 0 * * ?")
    public void autoTask() {
        System.out.println("触发自动收货");
        rabbitTemplate.convertAndSend("", "order_tack", "确认收货的定时任务");
    }
}
