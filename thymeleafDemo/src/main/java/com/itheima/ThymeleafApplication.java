package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/1
 * @description ：
 * @version: 1.0
 */
@SpringBootApplication
public class ThymeleafApplication {
    public static void main(String[] args) {
        SpringApplication.run(ThymeleafApplication.class, args);
    }
}
