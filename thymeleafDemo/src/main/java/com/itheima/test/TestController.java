package com.itheima.test;

import com.itheima.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

/**
 * @author ：liJingJie
 * @date ：Created in 2019/8/1
 * @description ：
 * @version: 1.0
 */
@Controller
@RequestMapping("test")
public class TestController {
    @RequestMapping
    public String hello(Model model) {
        model.addAttribute("hello", "欢迎来到这个世界!");
        return "demo1";
    }

    @PostMapping("/hello")
    public String hello(String id, Model model) {
        System.out.println("=========" + id);
        model.addAttribute("hello", id);
        List<User> userList = new ArrayList<>();
        userList.add(new User(1, "张三", "深圳"));
        userList.add(new User(2, "李四", "北京"));
        userList.add(new User(3, "王五", "武汉"));
        model.addAttribute("users", userList);
        //Map定义
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("No", "123");
        dataMap.put("address", "深圳");
        model.addAttribute("dataMap", dataMap);
        //存储一个数组
        String[] names = {"张三", "李四", "王五"};
        model.addAttribute("names", names);
        //日期
        model.addAttribute("now", new Date());
        //if条件
        model.addAttribute("age",22);
        return "demo1";
    }
}
